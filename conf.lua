love.conf = function(t)
   t.gammacorrect = true
   t.title, t.identity = "axion-scion", "Space Scion"
   t.modules.joystick = false
   t.modules.physics = false
   t.window.width = 960
   t.window.height = 720
   t.window.minwidth = 960
   t.window.minheight = 720   
   t.window.vsync = true
   t.version = "11.3"
   t.window.resizable=true
end
