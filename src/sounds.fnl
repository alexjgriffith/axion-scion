(local sounds {})

(local sources {})

(local {: web} (require :state))


(local audio
       (if web
           (love.audio.newSource "assets/music/Alexander Griffith - Melancholy.mp3" "static")
           (love.audio.newSource "assets/music/Alexander Griffith - Melancholy.mp3" "stream")))
(audio:setLooping true)
(audio:setVolume 0.25)
(local bgm (fn [] (audio:stop) (audio:play)))

(fn make-sound [name file ?volume]
  (let [volume (or ?volume 0.2)
        sound (love.audio.newSource file "static")]
    (sound:setVolume volume)
    (tset sources name sound)
    (tset sounds name (values (fn [] (sound:stop) (sound:play)) ))))

(fn make-sounds [t]
  (each [name [file ?volume] (pairs t)]
    (make-sound name file ?volume)))

(local sound-data {:click [ "assets/sounds/click.mp3"]
                   :page ["assets/sounds/page.ogg"]
                   :cash ["assets/sounds/cash.ogg"]
                   :bounce ["assets/sounds/bounce.ogg"]
                   :scratch ["assets/sounds/scratch.ogg"]
                   :land ["assets/sounds/land.ogg"]
                   :ding1 ["assets/sounds/bell_ding1.ogg" 0.1]
                   :ding2 ["assets/sounds/bell_ding2.ogg" 0.1]
                   :ding3 ["assets/sounds/bell_ding3.ogg" 0.1]
                   :ding4 ["assets/sounds/bell_ding4.ogg" 0.1]
                   :ding5 ["assets/sounds/bell_ding1.ogg" 0.1]
                   :ding6 ["assets/sounds/bell_ding2.ogg" 0.1]
                   :ding7 ["assets/sounds/bell_ding3.ogg" 0.1]
                   :ding8 ["assets/sounds/bell_ding4.ogg" 0.1]
                   :ding9 ["assets/sounds/bell_ding1.ogg" 0.1]
                   :ding10 ["assets/sounds/bell_ding2.ogg" 0.1]
                   :ding11 ["assets/sounds/bell_ding3.ogg" 0.1]
                   :ding12 ["assets/sounds/bell_ding4.ogg" 0.1]
                   })

(make-sounds sound-data)

(fn mute []
  (local state (require :state))
  (tset state :mute true)
  (each [name sound (ipairs sources)]
    (sound:setVolume 0))
  (audio:setVolume 0))

(fn unmute []
  (local state (require :state))
  (tset state :mute false)
  (each [name (_fn sound) (ipairs sounds)]
    (sound:setVolume (or (. sound-data name 2) 0.2)))
  (audio:setVolume 0.25))

{: sounds : bgm : mute : unmute}
