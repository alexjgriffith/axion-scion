;;(local axion-scion (fennel.dofile :axion-scion.fnl))

(local axion-scion (require :axion-scion))

(icollect [_ v (ipairs (axion-scion.draw 5))] (when v v.name))

(axion-scion.reset-state)

(axion-scion.event)

(axion-scion.start)

(axion-scion.get-strata-stats :prol)

(axion-scion.get-strata-stats :mill)

(axion-scion.get-scion-card)

;;[who cards-selected ?cards-returned]
(axion-scion.draw 5)

(axion-scion.select :player [(. (axion-scion.get-pot) 1) (. (axion-scion.get-pot) 3) (. (axion-scion.get-pot) 4)])

;; Bug not transfering between hands
;; be more explicit
(# (axion-scion.get-player-hand))


(# (axion-scion.get-pot))


(axion-scion.play (. (axion-scion.get-player-hand) 1))


(axion-scion.get-player-hand)


(axion-scion.get-active-card)


(axion-scion.resolve)


(axion-scion.clear-pot)


(axion-scion.event)

(axion-scion.reset-state)


(axion-scion.reset-state)

(axion-scion.event)

(axion-scion.start)


(var pot (axion-scion.draw 5))

(var player-hand nil)

(var active-card nil)

(set (player-hand pot) (axion-scion.select :player [(. pot 1) (. pot 3) (. pot 4)]))


(set (active-card player-hand) (axion-scion.play (. player-hand 3)))


(axion-scion.counter :player (. player-hand 1))


(axion-scion.resolve)


(axion-scion.end)

(axion-scion.get-hand :player)

(axion-scion.set-hands {:player [:rally-support :cut-spending :tax-loopholes]
                        :olig [:rally-support]})


(axion-scion.set-state {:who :player})

(axion-scion.play :cut-spending)


(axion-scion.counter :olig :rally-support)


(axion-scion.resolve)

;; todo fix counter
;; get hand setting working
;; get state setting working
