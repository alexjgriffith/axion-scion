(local directory "assets/icons/png/")

(local ending "target.png")

(local seperator "-")

(local names
       ["x" "o" "load" "fist" "prol" "oligarch" "military" "pitchfork" "communism" "fire" "stocks" "factory" "fighter" "sign"
        "gun" "ivy" "jack" "money" "up" "down" "palm" "bam" "bombs" "shot" "nuke" "throne" "pirate" "education" "insignia" "wine"
        "keg" "court" "meteor" "hammer" "knife" "tank" "note" "ballot" "glasses" "heart"
        "mute-old" "menu" "unmute-old" "fullscreen-old" "mute" "unmute" "fullscreen"])

(local sizes [16 20 32 64 128])

(local icons [])

(each [index name (ipairs names)]
  (when (not (. icons name))
    (tset icons name []))
  (each [_ size (ipairs sizes)]    
    (tset (. icons name) size
          (love.graphics.newImage (string.format "%s%s-%s-%s" directory size index ending)))))


(fn [name size]
  (. icons name size))
