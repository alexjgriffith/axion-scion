(local repl (require :lib.stdio))

(local pallet (require :pallet))

(local anchor (require :lib.anchor))

(local game-modes (require :lib.game-modes))

(local fade (require :fade))

(local {: title-font : text-font} (require :fonts))

(var clicked false)

(local {: sounds} (require :sounds))

(local state (require :state))

(local button (require :button))

(var over false)

(local {: story-text} (require :text))

(fn draw []
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))  
  (anchor.push-anchor 0 0 w h :center :center)
  (anchor.push-column 0 60 960 720 :center 10)    
  (anchor.push-element 0 0 960 60)  
  (love.graphics.setColor pallet.shadow)
  (love.graphics.setFont title-font)
  (love.graphics.printf "Opening" 100 0 (- 960 200) :center)
  (anchor.pop)
  (anchor.push-element 0 0 960 480)  
  (love.graphics.setFont text-font)
  (love.graphics.setColor pallet.shadow)
  (love.graphics.printf story-text 50 0 (- 960 100) :left)
  (anchor.pop)
  
  (local [o who]
         (-> [false nil]
             (button "Play Game"
                (fn []
                  (fade.set-color :shadow)
                  (fade.timed-fade-out 0.5
                                       (fn [] (game-modes.switch (require :mode-game))))) mx my clicked)))
  (when (~= o over)
    (set over o)
    (when over
      (let [opts [sounds.ding1 sounds.ding2 sounds.ding3 sounds.ding4]
            sound (. opts (math.random 1 4))]
        (sound)
        
        )))  
  (anchor.pop)
  (anchor.pop)
  (fade.draw w h)
  (set clicked false)
  )


(fn enter [self from arg]
  (tset state :story-played true)
  (love.graphics.setBackgroundColor pallet.light-blue)
  (fade.timed-fade-in 1))

(fn mousepressed [x y button] (set clicked true))

(fn update [self dt]
  (fade.update dt))

(local keybindings (require :keybindings))
(fn keypressed [self key]
  (match key
    _ (keybindings key)))

{: draw : update : keypressed : mousepressed : enter}
