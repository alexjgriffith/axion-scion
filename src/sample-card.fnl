(local {: card-draw : card-width : card-height} (require :render-card))

(local as (require :axion-scion))

(local margin 10)

(local offset (+ card-width margin))

(local offset-y (+ card-height margin))

(local cards (require :cards))

(var focus [false])

(local { : convert : cammel-to-capital} (require :utils))

;; add a header with turn#/turn max, instruction, and menu buttons
;; render strata on top, pot below and hands below that
;; played cards get put into the pot
;; scale on a canvas

(fn clicked [what mx my click x y w h ...]
  (local over (pointWithin mx my x y w h))
  (when over (set focus [what ...]))
  (when (and over click)
    (love.event.push :ui-clicked what ...)))

(fn draw-pot [dt mx my click]
  (local {: card-draw : card-width : card-height} (require :render-card))
  (local pot-width (+ (* 2 margin) (* 3 card-width)))

  (local pot-height (+ (* 1 margin) (* 2 card-height)))

  (var init-x (/ (- 960 pot-width) 2))
  (var init-y (/ (- 720 pot-height) 2))
  (each [i card (ipairs (as.get-hand :pot))]
    (when (<= i 5)
      (card-draw card mx my click init-x init-y :pot i)
      (when (= i 3)
        (set init-y (+ init-y offset-y))
        (set init-x (/ (- 960 pot-width card-width) 2)))
      (set init-x (+ init-x offset)))))

(fn draw-icon [icon mx my click x y w w? h? colour?]
  (local pallet (require :pallet))
  (local icons (require :icons))
  (local over (pointWithin mx my x y (or w? w) (or h? w)))
  (love.graphics.setColor
   (if over
       pallet.skin
       pallet.shadow
       ))
  (love.graphics.draw (icons icon w) x y)
  (when colour?
    (love.graphics.setColor colour?)
    (love.graphics.draw (icons icon w) x y))
  (love.graphics.setColor
   (if over
       pallet.skin
       pallet.shadow
       ))
  )

(fn draw-strata-info [dt mx my click who x y w h]
  (local pallet (require :pallet))
  (local font (require :fonts))
  (local {:who state-who } (require :state))
  (love.graphics.setColor pallet.light-blue)
  (when (or (= state-who who) (and (= who :ivy) (= state-who :player)))
      (love.graphics.setColor pallet.black))
 
  (love.graphics.rectangle :fill
                           x
                           y
                           w
                           h
                           10)
  
  (love.graphics.setColor pallet.light-blue)

  (love.graphics.rectangle :line
                           x
                           y
                           w
                           h
                           10)
  
  (love.graphics.rectangle :fill (+ x 5) (+ y 5) (- w 10) (- h 10) 10)
  
  (let [xp (+ x (/ (- w 32) 2))  yp (+ y 10) wp 32 hp 32]        
    (draw-icon who mx my click xp yp hp)
    (clicked who mx my click xp yp wp hp))
  (when (. convert who)
    (local stats (as.get-strata-stats (. convert who)))
    (when stats.has-scion
      (let [xp (+ x w -18)  yp (+ y h -40) wp 16 hp 16]
        (clicked :jack mx my click xp yp wp hp who)
        (draw-icon :jack mx my click xp yp wp)))
    (when stats.scion-loves
      (let [xp (+ x w -18)  yp (+ y h -18) wp 16 hp 16]
        (clicked :love mx my click xp yp wp hp who)
        (draw-icon :heart mx my click xp yp wp)))
    
    (love.graphics.setFont font.card-chance-font)
    (local state (require :state))
    (let [xp (+ x 10) yp (+ y 50) wp (- w 40) hp 20]
      (clicked :power mx my click xp yp wp hp who)
      (draw-icon :fist mx my click xp yp hp wp nil
                 (. state.highlight-value who :power)))
    (love.graphics.printf (math.floor stats.power) x (+ y 50) w :center)
    
    (let [xp (+ x 10) yp (+ y 75) wp (- w 40) hp 20]
      (clicked :content mx my click xp yp wp hp who)
      (draw-icon :heart mx my click xp yp hp wp nil
                 (. state.highlight-value who  :content)))
    (love.graphics.printf (math.floor stats.content) x (+ y 72) w :center))
)

(fn draw-military [dt mx my click]
  (local hand-width (+ (* 0 margin) (* 1 card-width)))
  (local hand-height (+ (* 1 margin) (* 2 card-height)))
  (var init-x (* 1 margin))
  (var init-y (+ (/ (- 720 hand-height) 2) 110))    
  (local {: light-blue} (require :pallet))
  (love.graphics.setColor light-blue)
  (love.graphics.rectangle :line
                           (- init-x (* 0.5 margin))
                           (- init-y (+ 30))
                           (+ hand-width (* 1 margin))
                           (+ hand-height (* 1 40))
                           10)
  (draw-strata-info dt mx my click :military init-x (- init-y 110) hand-width 100)
  (each [i card (ipairs (as.get-hand (. convert :military)))]
    (when (<= i 2)
      (card-draw card mx my click init-x init-y :military i)
      (set init-y (+ init-y offset-y)))))

(fn draw-prol [dt mx my click]
  (local {: card-draw : card-width : card-height} (require :render-card))
  (local hand-width (+ (* 0 margin) (* 1 card-width)))
  (local hand-height (+ (* 1 margin) (* 2 card-height)))
  (var init-x (- 960 hand-width margin))
  (var init-y (+ (/ (- 720 hand-height ) 2) 110))  
  (local {: light-blue} (require :pallet))
  (love.graphics.setColor light-blue)
  (love.graphics.rectangle :line
                           (- init-x (* 0.5 margin))
                           (- init-y (+ 30))
                           (+ hand-width (* 1 margin))
                           (+ hand-height (* 1 40))
                           10)
  (draw-strata-info dt mx my click :prol init-x (- init-y 110) hand-width 100)
  (each [i card (ipairs (as.get-hand (. convert :prol)))]
    (when (<= i 2)
      (card-draw card mx my click init-x init-y :prol i)
      (set init-y (+ init-y offset-y))
      )))

(fn draw-oligarch [dt mx my click]
  (local {: card-draw : card-width : card-height} (require :render-card))
  (local hand-width (+ (* 1 margin) (* 2 card-width)))
  (local hand-height (+ (* 0 margin) (* 1 card-height)))
  (var init-x (+ (/ (- 960 hand-width -10 (- card-width)) 2)))
  (var init-y margin)
  (local {: light-blue} (require :pallet))
  (love.graphics.setColor light-blue)
  (love.graphics.rectangle :line
                           (- init-x (+ (* 1 card-width) (- (* 1 margin))))
                           (- init-y (* margin 0.5))
                           (+ hand-width (* 1 card-width))
                           (+ hand-height (* 1 margin))
                           10)
  (draw-strata-info dt mx my click :oligarch
                    (- init-x 10 card-width) (+ init-y (/ (- card-height 100) 2)) card-width 100)
  (each [i card (ipairs (as.get-hand (. convert :oligarch)))]   
    (when (<= i 2)
      (card-draw card mx my click init-x init-y :oligarch i)
      (set init-x (+ init-x offset))
      )))

(fn draw-player [dt mx my click]
  (local {: card-draw : card-width : card-height} (require :render-card))
  (local hand-width (+ (* 3 margin) (* 4 card-width)))
  (local hand-height (+ (* 0 margin) (* 1 card-height)))
  (var init-x (/ (- 960 hand-width) 2))
  (var init-y (- 720 hand-height (* 1 margin)))    
  (local {: light-blue} (require :pallet))
  (love.graphics.setColor light-blue)
  (love.graphics.rectangle :line (- init-x margin)
                           (- init-y margin margin)
                           (+ hand-width margin margin) (+ hand-height (* 2.5 margin))
                           10)

  (draw-strata-info dt mx my click :ivy init-x (- init-y 60) card-width 50)
  
  (each [i card (ipairs (as.get-hand :player))]
    (when (<= i 4)
      (card-draw card mx my click init-x init-y :player i)
      (set init-x (+ init-x offset)))))

(fn draw-event [dt mx my click]
  (local {: card-draw : card-width : card-height} (require :render-card))
  (local card (as.get-event-card))
  (local init-x (- 960 card-width margin))
  (local init-y margin)
  (local {: light-blue} (require :pallet))
  (love.graphics.setColor light-blue)
  (love.graphics.rectangle :line
                           (- init-x (* 0.5 margin))
                           (- init-y (* margin 0.5))
                           (+ card-width (* 1 margin))
                           (+ card-height (* 1 margin))
                           10)
  (local state (require :state))
  (when (~= state.state :draw)
    
    (when card
      (card-draw (. card :name) mx my click init-x init-y :event 1))))

(local fonts (require :fonts))

(fn draw-state [dt mx my click]
  (local state (require :state))
  (local init-x margin)
  (local init-y margin)
  (local offset-x 30)
  (local offset-y 10)
  (local {: light-blue : shadow} (require :pallet))
  (love.graphics.setColor light-blue)
  (love.graphics.rectangle :fill init-x init-y 230 card-height 10)
  (love.graphics.setFont fonts.card-button-font)
  (love.graphics.setColor shadow)
  (love.graphics.printf (string.format "   State" state.turn)
                        (+ offset-x margin init-x) (+ offset-y margin init-y) 230 )
  (love.graphics.setFont fonts.card-button-font)
  (love.graphics.printf (string.format "Turn: %s/9" state.turn)
                        (+ offset-x margin init-x) (+ offset-y 30 margin init-y) 230 )
  (love.graphics.printf (string.format "Step: %s" (cammel-to-capital (if (~= :timer state.state)
                                                      state.state
                                                      state.ps)))
                        (+ offset-x margin init-x) (+ offset-y 60 margin init-y) 230 )
  (love.graphics.printf (string.format " Who: %s" (cammel-to-capital state.who))
                        (+ offset-x margin init-x) (+ offset-y 90 margin init-y) 230 )
  )


(fn action-button-colour [mx my x y w h]
  (local {:hot-pink background
          :skin hover} (require :pallet))
  (local over (pointWithin mx my x y w h))
  (love.graphics.setColor (if over hover background)))

(fn action-text-colour [mx my x y w h]
  (local {:shadow background
          :light-blue hover} (require :pallet))
  (local over (pointWithin mx my x y w h))
  (love.graphics.setColor (if over hover hover)))

(fn ui-button-colour [mx my x y w h]
  (local {:light-blue background
          :skin hover} (require :pallet))
  (local over (pointWithin mx my x y w h))
  (love.graphics.setColor (if over hover background)))

(fn ui-text-colour [mx my x y w h]
  (local {:shadow background} (require :pallet))
  (local over (pointWithin mx my x y w h))
  (love.graphics.setColor (if over background background)))

(fn draw-button [dt mx my click]
  (local state (require :state))  
  (local holding (> (# state.player-selected) 1))
  (local e-map {:Resolve :Next :Play :Play :Next :Next :Select :Select :Counter :Counter})
  (when (= state.who :player)
    (local init-x (- 960 card-width margin))
    (local init-y (- 720 50 margin))
    (action-button-colour mx my init-x init-y card-width 50)
    (love.graphics.rectangle :fill init-x init-y card-width 50 10)
    (action-text-colour mx my init-x init-y card-width 50)
    (love.graphics.setFont fonts.card-button-font)
    (love.graphics.printf (string.upper (or (. e-map state.player-action) state.player-action))
                          init-x (+ 12 init-y) card-width :center)
    (clicked :action mx my click init-x init-y card-width 50 state.player-action)))

(fn draw-ui [dt mx my click]
  (var init-x (/ margin 2))
  (var init-y (- 720 50 margin))
  (local icons (require :icons))
  (local {: mute } (require :state))
  
  (love.graphics.setFont fonts.card-button-font)
  (ui-button-colour mx my init-x init-y 50 45)
  (love.graphics.rectangle :fill init-x init-y 45 50 10)
  (ui-text-colour mx my init-x init-y 50 45)
  ;; (love.graphics.printf :M init-x (+ 10 init-y) 45 :center)
  (clicked :menu mx my click init-x init-y 50 45)
  (love.graphics.draw (icons :menu 32) (+ 6 init-x) (+ 10 init-y))
  
  (set init-x (+ init-x 52.5))  
  (ui-button-colour mx my init-x init-y 50 45)
  (love.graphics.rectangle :fill init-x init-y 45 50 10)
  (ui-text-colour mx my init-x init-y 50 45)
  ;;(love.graphics.printf :m init-x (+ 10 init-y) 45 :center)
  (clicked :mute mx my click init-x init-y 50 45)
  
  (love.graphics.draw (icons (if mute :unmute :mute) 32) (+ 6 init-x) (+ 10 init-y))
  
  (set init-x (+ init-x 55))
  (ui-button-colour mx my init-x init-y 50 45)
  (love.graphics.rectangle :fill init-x init-y 45 50 10)
  (ui-text-colour mx my init-x init-y 50 45)
  (love.graphics.draw (icons :fullscreen 32) (+ 6 init-x) (+ 10 init-y))
  ;; (love.graphics.printf :F init-x (+ 10 init-y) 45 :center)
  (clicked :full mx my click init-x init-y 50 45)
  )

(fn draw-active [dt mx my click]
  (local {: card-draw : card-width : card-height} (require :render-card))
  (local [card] (as.get-hand :active))
  (local pot (as.get-hand :pot))
  (local init-x (/ (- 960 card-width) 2))
  (local init-y (/ (- 720 card-height) 2))
  (when true
    (local {: light-blue} (require :pallet))
    (love.graphics.setColor light-blue)
    (love.graphics.rectangle :line
                             (- init-x (* 0.5 margin))
                             (- init-y (* margin 0.5))
                             (+ card-width (* 1 margin))
                             (+ card-height (* 1 margin))
                             10))
  (when card
    (card-draw card mx my click init-x init-y :active 1))
  )

(fn draw [dt mx my click]
  (local state (require :state))
  (set focus [false])
  (draw-active dt mx my click)
  (draw-pot dt mx my click)
  (draw-player dt mx my click)
  (draw-military dt mx my click)
  (draw-prol dt mx my click)
  (draw-oligarch dt mx my click)
  (draw-event dt mx my click)
  (draw-state dt mx my click)
  (draw-button dt mx my click)
  (draw-ui dt mx my click)
  focus
  )
