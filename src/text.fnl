(local story-text "Welcome to Axion Prime. You are our beloved despot. 

The years of ruling tyrannically have tuckered you out, and you are looking for an exit. A nice paradise planet would suit you perfectly. Fortunately for you your Scion has just come of age!

If you can maintain the balance of power and keep your scion alive for 9 years they will be able to fill your shoes.")


(local collage "Education by labour for most, but for your Scion only the finest University will do. For the next three turns they will be hosted by the proletariat (Prols). 

Prols will have a 1.5 times multiplier when making counters. 

Any changes to the Power of the Prols will be multiplied by 1.5.")

(local meteor "It looks like a meteor is headed straight for Axion Prime!

If the military can shoot it down it will look good for them. Everyone else very concerned, rightfully so.")

(local graduation "From the balcony of the graduation ceremony you hear the phrase 'Labouring brains leverage freedom to prosperity'. You have no idea what it means, but it sound tyrannical enough for the tuition payments to be worth it.

With your Scion absent the Proletariat is greatly weakened. They are not happy about this")

(local service "Military service comes to us all. If you are the Scion of Axion it's more like the military serves you only. For the next three turns your scion will be hosted by the Military.

The Military will have a 1.5 times multiplier when making counters. 

Any changes to the Power of the Military will be multiplied by 1.5.")

(local pirate-attack "There are pirates in your Space Space! This is an embarrassment for your Military.")

(local market-booms "Turns out those pirates were just wayward traders. Good thing you had them shot down and their goods seized. The quality is beyond anything produced in Axion. The markets are bombing at the potential! It is truly the age of the oligarchs!")

(local elected "To lead is the breed, or something like that, its a long time since your university days. In any case, your offspring now schmoozes with the oligarchs. They have been recently elected to the seat of vice regent, not to bad for the child of a despot.

The Oligarchs will have a 1.5 times multiplier when making counters. 

Any changes to the Power of the Oligarchs will be multiplied by 1.5.")

(local fission-fiasco "Like clockwork the bombs have been dropped. These accidents are really getting out of hand. Fortunately there were no observers left alive.")

(local planet-cleared "News has reached our sector that a far off tropical planet just disappeared from the star charts. How does someone lose a planet they all ask one another. It looks like your plans is close to fruition.")

(local end-scion-text
       "Dare to dream no more...

With the passing of your Scion your dreams of retirement have faded. There is nothing left for you to do but double down on your tyrannically governance. Surely that will bring balance.")

(local end-player-text
       "Good news everyone!

Ever since your head and body parted ways you've been much less stressed about being a tyrannical despot. You've been much less stressed about everything actually, which has been GREEEEAT!
")

(local end-win-text
       "You've done it!

You managed to keep your Scion alive and they've navigated the currents of politics to fill your bloody shoes. You have a nice little planet picked out for your retirement.")
;; Hopefully those peasant whispers you've been hearing in the halls don't amount to anything.

{: story-text
 : end-scion-text
 : end-player-text
 : end-win-text
 : pirate-attack
 : service
 : elected
 : graduation
 : meteor
 : collage
 : fission-fiasco
 : market-booms
 : planet-cleared
 }
