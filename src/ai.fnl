(local ai {})


(local as (require :axion-scion))
(local cards (require :cards))

(fn highest-value [hand who]
  (local cs (icollect [_ c (ipairs hand)] (. cards c)))
  (var card false)
  (var index false)
  (var max 0)  
  (each [i c (ipairs cs)]
    (local value (?. c.ai who))
    (when (and value (> value max))
      (set max value)
      (set index i)
      (set card c.name)))
  (values card index))

(fn highest-value-playable [hand who]
  (local cs (icollect [_ c (ipairs hand)] (. cards c)))
  (pp cs)
  (var card false)
  (var index false)
  (var max 0)
  (each [i c (ipairs cs)]
    (local value (?. c.ai who))
    (when (and value (> value max) (~= c.type :counter))
      (set max value)
      (set index i)
      (set card c.name)))
  (values card index))

(fn highest-value-counter [hand who]
  (local cs (icollect [_ c (ipairs hand)] (. cards c)))
  (var card false)
  (var index false)
  (var max 0)
  (var worth-countering false)
  (local [active] (as.get-hand :active))
  (local a (. cards active))
  (var value (?. a :ai who))
  (when (or (not value) (and value (< value 0)))
    (set worth-countering true))
  (when worth-countering
    (each [i c (ipairs cs)]
      (local chance (or (?. c :chance) 1))
    (when (and (> chance max) (= c.type :counter))
      (set max chance)
      (set index i)
      (set card c.name))))
  (values card index))

(fn ai.select [who whoas]
  (let [pot (as.get-hand :pot)]
    (let [(card-st index) (highest-value pot whoas)]
      (when index
        (love.event.push :card-clicked :pot index 1)))
    )
  (love.event.push :ui-clicked :action :Select))

(fn ai.play [who whoas]
  (local hand (as.get-hand whoas)) 
  (local (card-st index) (highest-value-playable hand whoas))
  (when index
    (love.event.push :card-clicked who index 1))
  (love.event.push :ui-clicked :action :Play)
  true)

(fn ai.counter [who whoas]
  (local hand (as.get-hand whoas))
  (local (card-st index) (highest-value-counter hand whoas))
  (pp [who card-st index])
  (when index
    (love.event.push :card-clicked who index 1))
    (love.event.push :ui-clicked :action :Counter)
  true)

ai
