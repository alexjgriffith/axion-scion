(local text (require :text))

(local description (require :description))

(local soft-intervention-power 25)

(local hard-intervention-power 40)

{
 ;; scion
 :hostage
 {:name :hostage
  :type :scion
  :chance 0.25
  :weight 1
  :icon :knife
  :strata :prol
  :content 30
  :ai {:mill 0 :olig 0 :prol 0.5}
  :effects [{:type :strata :effect [:prol :power 20]}
            {:type :strata :effect [:prol :content 40]}
            {:type :scion :effect [:alive false]}
            ]}

 :new-friends
 {:name :new-friends
  :type :scion
  :chance 0.25
  :weight 1
  :icon :wine
  :strata :olig
  :content 30
  :ai {:mill 0 :olig 0.5 :prol 0}
  :effects [{:type :strata :effect [:olig :power 20]}
            {:type :strata :effect [:olig :content 40]}
            {:type :scion :effect [:alive false]}
            ]}

 :exercises
 {:name :exercises
  :type :scion
  :chance 0.25
  :weight 1
  :icon :wine
  :strata :mill
  :content 30
  :ai {:mill 0.5 :olig 0 :prol 0}
  :effects [{:type :strata :effect [:olig :power 20]}
            {:type :strata :effect [:olig :content 40]}
            {:type :scion :effect [:alive false]}
            ]}

 :romance
 {:name :romance
  :type :scion
  :chance 0.9
  :weight 1
  :icon :heart
  :strata :prol
  :ai {:mill 0 :olig 0 :prol 2}
  :effects [{:type :scion :effect [:love :prol]}
            ]}

 :aisle-crosser
 {:name :aisle-crosser
  :type :scion
  :chance 0.9
  :weight 1
  :icon :heart
  :strata :olig
  :ai {:mill 0 :olig 2 :prol 0}
  :effects [{:type :scion :effect [:love :olig]}]}

 :cozy-foxhole
 {:name :cozy-foxhole
  :type :scion
  :chance 0.9
  :weight 1
  :icon :heart
  :strata :mill
  :ai {:mill 2 :olig 0 :prol 0}
  :effects [{:type :scion :effect [:love :mill]}]}

 ;; events
 :university
 {:name :university
  :type :scion-event
  :icon :education
  :title "Scion Goes to Collage"
  :text text.collage
  :effects [{:type :scion :effect [:where :prol]}]
  :event 1}

 :meteor
 {:name :meteor
  :type :cataclysm
  :icon :meteor
  :title "A Meteor Approaches"
  :text text.meteor
  :effects [{:type :strata :effect [:mill :power 10]}
            {:type :strata :effect [:prol :content -20]}
            {:type :strata :effect [:olig :content -20]}]
  :event 2}

 :graduation
 {:name :graduation
  :type :scion-event
  :icon :education
  :title "Graduation"
  :text text.graduation
  :effects [{:type :strata :effect [:prol :power -20]}
            {:type :strata :effect [:prol :content -10]}
            {:type :strata :effect [:olig :content 20]}]
  :event 3}
 
 :service
 {:name :service
  :type :scion-event
  :icon :insignia
  :title "Scion Joins the Military"
  :text text.service
  :effects [{:type :scion :effect [:where :mill]}]
  :event 4}

 :pirate-attack
 {:name :pirate-attack
  :type :cataclysm
  :icon :pirate
  :title "Pirates Have Attacked!"
  :text text.pirate-attack
  :effects [{:type :strata :effect [:mill :power -10]}
            {:type :strata :effect [:prol :content -10]}
            {:type :strata :effect [:olig :content -10]}]
  :event 5}

 :market-booms
 {:name :market-booms
  :type :jubilation
  :icon :stocks
  :title "Bull Markets Reign"
  :text text.market-booms
  :effects [{:type :strata :effect [:olig :power 20]}]
  :event 6}
 
 :elected
 {:name :elected
  :type :scion-event
  :icon :wine
  :title "Scion Elected to the Governing Board"
  :text text.elected
  :effects [{:type :scion :effect [:where :olig]}
            {:type :strata :effect [:prol :content 20]}
            {:type :strata :effect [:olig :content 20]}]
  :event 7}

 :fission-fiasco
 {:name :fission-fiasco
  :type :cataclysm
  :icon :nuke
  :title "Military Incompetence and the Bomb"
  :text text.fission-fiasco
  :effects [{:type :strata :effect [:mill :power -20]}
            ]
  :event 8}

 :planet-cleared
 {:name :planet-cleared
  :type :mystery
  :icon :o
  :title "Paradise Planet Disappears"
  :text text.planet-cleared
  :effects [{:type :strata :effect [:olig :content -10]}
            {:type :strata :effect [:mill :content -10]}
            {:type :strata :effect [:prol :content -10]}]
  :event 9} 
 
 ;; interverions
 
 :soft-coup {:name :soft-coup
             :type :intervention
             :chance  0.5
             :effects [{:type :player :effect [:alive false]}]
             :icon :tank
             :strata :mill
             :weight 20
             :power soft-intervention-power
             :content 50
             :ai {:mill 100}
             }
 
 :hard-coup {:name :hard-coup
             :type :intervention
             :chance  0.9
             :effects [{:type :player :effect [:alive false]}]
             :strata :mill
             :icon :tank
             :weight 100
             :power hard-intervention-power
             :content 20
             :ai {:mill 1000}
             }

 :revolt {:name :revolt
          :type :intervention
          :chance  0.5
          :effects [{:type :player :effect [:alive false]}]
          :icon :pitchfork
          :strata :prol
          :weight 20
          :power soft-intervention-power
          :content 50
          :ai {:prol 100}
          }
 
 :revolution {:name :revolution
              :type :intervention
              :chance  0.9
              :effects [{:type :player :effect [:alive false]}]
              :strata :prol
              :icon :communism
              :weight 100
              :power hard-intervention-power
              :content 20
              :ai {:prol 1000}
              }
 
 :power-play
 {:name :power-play
  :type :intervention
  :chance  0.5
  :effects [{:type :player :effect [:alive false]}]
  :icon :wine
  :strata :olig
  :weight 20
  :power soft-intervention-power
  :content 50
  :ai {:olig 100}
  }
 
 :deadly-gala
 {:name :deadly-gala
  :type :intervention
  :chance  0.9
  :effects [{:type :player :effect [:alive false]}]
  :strata :olig
  :icon :wine
  :weight 100
  :power hard-intervention-power
  :content 20
  :ai {:olig 1000}
  }

 ;; counters
 :rally-support {:name :rally-support
                 :type :counter
                 :chance 0.33
                 :weight 4
                 :icon :x
                 :ai {:mill 1 :olig 1 :prol 1}
                 }

 :strike {:name :strike
          :type :counter
          :strata :prol
          :chance 0.5
          :weight 1
          :icon :sign
          :ai {:prol 5}}

 :lobby {:name :lobby
         :type :counter
         :strata :olig
         :chance 0.5
         :weight 1
         :icon :money
         :ai {:olig 5}}

 :fly-by {:name :fly-by
          :type :counter
          :strata :mill
          :chance 0.5
          :weight 1
          :icon :fighter
          :ai {:mill 5}}

 ;; policy
 :devolve-power {:name :devolve-power
                 :type :policy
                 :chance 1
                 :weight 1
                 :icon :court
                 :strata :olig
                 :effects [{:type :strata :effect [:olig :power 10]}
                           {:type :strata :effect [:olig :content 5]}
                           {:type :strata :effect [:mill :content -10]}
                           {:type :strata :effect [:prol :content -10]}]
                 :ai {:olig 1 :mill -1 :prol -1}}

 :centralize-power {:name :centralize-power
                    :type :policy
                    :chance 1
                    :weight 1
                    :icon :court
                    :strata :olig
                    :effects [{:type :strata :effect [:olig :power -10]}
                              {:type :strata :effect [:olig :content -5]}
                              {:type :strata :effect [:mill :content 10]}
                              {:type :strata :effect [:prol :content 10]}]
                    :ai {:olig -1 :mill 1 :prol 1}}

 
 :promote-general {:name :promote-general
                   :type :policy
                   :chance 1
                   :weight 1
                   :strata :mill
                   :icon :fist
                   :effects [{:type :strata :effect [:mill :power 10]}
                             {:type :strata :effect [:mill :content 5]}
                             {:type :strata :effect [:olig :content -10]}
                             {:type :strata :effect [:prol :content -10]}]
                   :ai {:mill 1 :olig -1 :prol -1}}

 :support-union {:name :support-union
                 :type :policy
                 :chance 1
                 :weight 1
                 :icon :factory
                 :strata :prol
                 :effects [{:type :strata :effect [:prol :power 10]}
                           {:type :strata :effect [:prol :content 10]}
                           {:type :strata :effect [:olig :content -10]}]
                 :ai {:prol 1 :olig -1 :mill -1}}

 :bust-union {:name :bust-union
              :type :policy
              :chance 1
              :weight 1
              :icon :factory
              :strata :olig
              :effects [{:type :strata :effect [:prol :power -10]}
                        {:type :strata :effect [:olig :content 10]}
                        {:type :strata :effect [:prol :content -20]}]
              :ai {:olig 1 :prol -1 :mill -1}}

 :military-parade {:name :military-parade
                   :type :policy
                   :icon :tank
                   :chance 1
                   :weight 1
                   :strata :mill
                   :effects [{:type :strata :effect [:mill :content 20]}
                             {:type :strata :effect [:mill :power 10]}
                             {:type :strata :effect [:prol :content 5]}
                             {:type :strata :effect [:olig :power -5]}]
                   :ai {:olig -1 :prol -1 :mill 1}
                   }

 :civic-holiday {:name :civic-holiday
                :type :policy
                :chance 1
                :weight 1
                :icon :keg
                :strata :prol
                :effects [{:type :strata :effect [:prol :content 20]}
                          {:type :strata :effect [:prol :power 10]}
                          {:type :strata :effect [:olig :content -10]}]
                :ai {:olig -1 :mill -1 :prol 1}
                }

 :cancel-holiday {:name :cancel-holiday
                :type :policy
                :chance 1
                :weight 1
                :icon :keg
                :strata :prol
                :effects [{:type :strata :effect [:prol :content -20]}
                          {:type :strata :effect [:olig :content 20]}]
                :ai {:olig 1 :mill -1 :prol -1}
                }
 
 :tax-loopholes {:name :tax-loopholes
                 :type :policy
                 :chance 1
                 :weight 1
                 :icon :money
                 :strata :olig
                 :effects [{:type :strata :effect [:olig :content 20]}
                           {:type :strata :effect [:olig :power 10]}]
                 :ai {:prol -1 :mill -1 :olig 1}
                 }

 :conscription {:name :conscription
                :type :policy
                :chance 1
                :weight 1
                :icon :fist
                :strata :mill
                :effects [{:type :strata :effect [:mill :power 20]}
                          {:type :strata :effect [:mill :content 20]}
                          {:type :strata :effect [:prol :content -40]}]
                :ai {:prol -1 :mill 1 :olig -1}}

 :cut-spending {:name :cut-spending
                :type :policy
                :chance 1
                :weight 1
                :icon :money
                :strata :olig
                :effects [{:type :strata :effect [:mill :power -20]}
                          {:type :strata :effect [:mill :content -40]}
                          {:type :strata :effect [:olig :content 40]}
                          {:type :strata :effect [:prol :content 10]}]
                :ai {:mill -10 :olig 1 :prol -1}}}
