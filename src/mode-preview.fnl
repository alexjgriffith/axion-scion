(local repl (require :lib.stdio))
(local pallet (require :pallet))
(local game-modes (require :lib.game-modes))
(local cards (require :cards))
(local {: card-draw : card-width : card-height} (require :render-card))

(local flux (require :lib.flux))

(local lg love.graphics)

(local icons (require :icons))

(local card-names (icollect [name _ (pairs cards)] name))

(local canvases [(lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)
                 (lg.newCanvas card-width card-height)])

(fn random-card []
  (. card-names (math.random 1 (# card-names))))

(local card-render-info {})

(local fg (flux.group))

(local x-starts [(* 2 card-width) (* 8 card-width) (* 3.5 card-width) (* -0.5 card-width) (* 6.5 card-width) (* 0.5 card-width) (* 5 card-width)])

(var x-index 1)

;; 780 x 405

;; 

(local flux-time 0.75)

(fn add-random-card []
  (local card {:x (. x-starts x-index) :y 720 :colour [1 1 1 1]
               :card (random-card) :canvas (. canvases x-index)})  
  (local i x-index)
  (tset card-render-info i card)
  (fg:to (. card-render-info i) flux-time {:y 600})
  (fg:to (. card-render-info i :colour) flux-time {4 1})
  (set x-index (+ x-index 1))
  (when (> x-index (# x-starts))
    (set x-index 1)))

(local main-canvas (lg.newCanvas 1200 720))



(fn draw []
  (lg.setCanvas main-canvas)
  (lg.clear)
  (local {:big-font font} (require :fonts))
  (local (w h) (love.window.getMode))
  (local fh (font:getHeight))
  (lg.setColor pallet.light-blue)  
  (love.graphics.setLineWidth 1)    
  (each [_ c (ipairs card-render-info)]
    (when (and c.x c.y c.colour c.card)
      (lg.push)
      (lg.setCanvas c.canvas)      
      (card-draw c.card 0 0 false 0 0)
      (lg.setCanvas main-canvas)
      (lg.setColor c.colour)      
      (lg.draw c.canvas c.x c.y 0 2)
      (lg.pop)
      )
    
    )
  
  (lg.setColor pallet.light-blue)
  (lg.setFont font)
  (lg.printf "Axion Scion" 0 (/ (- h fh) 2) 960 :center)
  (love.graphics.draw (icons :ivy 128) (/ (- 960 128) 2) 100)  
  (lg.setCanvas)
  (lg.push)
  (lg.setColor 1 1 1 1)
  (lg.scale 0.75)
  (lg.draw main-canvas)
  (lg.pop)
  
  (love.graphics.setLineWidth 30)
  ;; (lg.setColor pallet.light-blue)
  ;; (love.graphics.rectangle :line 55 0 630 500)
  )


(var create-cards false)

(fn toggle-cards []
  (set create-cards (not create-cards)))

(var timer 2)
(local period 0.1)
(fn update [obj dt]
  (fg:update dt)
  (when create-cards
    (set timer (+ timer dt))
  (when (and (> timer period))
    (add-random-card)
    (set timer 0)))
  ;; (pp obj)  
  )


(var background pallet.light-blue)
(fn enter [obj]
  (set background [(love.graphics.getBackgroundColor)])
  (lg.setBackgroundColor pallet.light-blue))

(fn leave [obj]
  (lg.setBackgroundColor background))


(fn keypressed [obj key]
  (local keybindings (require :keybindings))
  (match key
    := (toggle-cards))
  (keybindings key))

{: draw : update : keypressed : enter : leave}
