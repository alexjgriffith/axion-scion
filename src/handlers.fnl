(local state (require :state))

(local fade (require :fade))

(local game-modes (require :lib.game-modes))

(local {: sounds} (require :sounds))

(local {: member : unconvert : convert} (require :utils))
(local as (require :axion-scion))

(local cards (require :cards))

(fn view-card [who index key]
  (pp [who index key])
  (pp who)
  (when (= key 2)
    (fade.set-color :light-blue)
    (fade.timed-fade-out
     0.5
     (fn []       
       (local as (require :axion-scion))
       (set state.re-enter :game)
       (game-modes.push (require :view-card) (. who index))))))

(fn add-remove [ar st to key]
  (if (= key 1)
      (if (member ar st)
          (lume.remove ar st)
          (if to
              (tset ar to st)
              (table.insert ar st)))      
      ))

;; add checks for count and type

(fn check-state [where index]
  (local {:state state-state : who} state)  
  (local selected
         (match who
           :player state.player-selected
           :oligarch state.oligarch-selected
           :military state.military-selected
           :prol state.prol-selected
           _ []))  
  (local pot state.pot-selected)
  (local hand (as.get-hand (or (. convert who) who)))
  (local pot-hand (as.get-hand :pot))
  (local count-in-hand (# hand))
  (local count-in-pot (# pot-hand))
  (local count-selected (# selected))
  (local pot-selected (# pot))
  (match [who state-state where]
    [:player :select :pot] (or (< (+ pot-selected count-in-hand (- count-selected))
                                  4)
                               (member pot index))
    [_ :select :pot] (or (< (+ pot-selected count-in-hand (- count-selected))
                            2)
                         (member pot index))
    [_ :select _] (< (+ count-in-pot  (- pot-selected) count-selected)
                     5)        
    _ true
    )
  )

(fn check-type [where index]
  (local {:state state-state : who} state)
  (local hand (as.get-hand (or (. convert where) where)))
  (local card (. cards (. hand index)))
  (local type card.type)
  (match state-state
    :select true
    :counter (= type :counter)
    :play (~= type :counter)
    _ false)
  )


(fn love.handlers.card-clicked [where index key]
  (pp [:card-clicked where index key])
  (view-card (as.get-hand (or (. convert where) where)) index key)
  (when (and (check-state where index) (check-type where index))
    (local {:state state-state} state)
    (local to (match state-state :select nil :play 1 :counter 1))
    (sounds.click)
    (local as (require :axion-scion))
    (match where
      :pot (do (add-remove state.pot-selected index nil key)
               ;; (view-card (as.get-hand :pot) index key)
               )
      :player (when (= :player state.who)
                ;; (view-card (as.get-hand :player) index key)
                (add-remove state.player-selected index to key))
      :oligarch (when (= :oligarch state.who)
                  ;; (view-card (as.get-hand :oligarch) index key)
                  (add-remove state.oligarch-selected index to key))
      :military (when (= :military state.who)
                  ;; (view-card (as.get-hand :military) index key)
                  (add-remove state.military-selected index to key))
      :prol (when (= :prol state.who)
              ;; (view-card (as.get-hand :prol) index key)
              (add-remove state.prol-selected index to key))
      )
    )
  )

(fn get-who-cards [who]
  (fn get [loc who]
    (let [which (. state loc)
                  hand (as.get-hand who)]
              (icollect [_ i (ipairs which)]
                (. hand i))))
  (match who
    :player (get :player-selected who)
    :pot (get :pot-selected who)
    :oligarch (get :oligarch-selected :olig)
    :military (get :military-selected :mill)
    :prol (get :prol-selected who)))

(fn unselect-all []
  (tset state :player-selected [])
  (tset state :military-selected [])
  (tset state :prol-selected [])
  (tset state :oligarch-selected [])
  (tset state :pot-selected []))

(var counters [])

(fn action [t]
  (local who state.who)
  (match t
    :Draw (do (fn callback [] (tset state :state :select)              
              (tset state :player-selected [])
              (tset state :pot-selected [])
              (tset state :player-action :Select)
              (tset state :order (icollect [_ o (ipairs (as.order))] (or (. unconvert o) o)))              
              (tset state :order-index 1)
              (tset state :who (. state.order state.order-index))
              (as.set-hand :pot [])
              (as.draw 5))
              (local event-offset (require :event-offset))
              (event-offset.raise 0.5 callback)
              (sounds.page)
              )
    :Select (do
              (var move-to-play false)
              (let [pot-cards (get-who-cards :pot)
                    hand-cards (get-who-cards state.who)]
                (as.select-return-cards (or (. convert who) who) pot-cards hand-cards))
              (unselect-all)
              (tset state :order-index (+ state.order-index 1))
              (when (> state.order-index 4)
                (as.set-hand :pot [])
                (tset state :order-index 1)
                (tset state :state :play)
                (tset state :player-action :Play)
                (set move-to-play true))
              (tset state :who (. state.order state.order-index)))
    :Play (do
            (local card (. (get-who-cards who) 1))
            (if card
                (do (as.play (or (. convert who) who) card)
                    (unselect-all)
                    (tset state :state :counter)
                    (tset state :player-action :Counter)
                    (tset state :counter-point state.order-index)
                    (if (> (+ 1 state.order-index) 4)
                        (do (tset state :counter-index 1)
                            (tset state :who (. state.order 1)))
                        (do (tset state :counter-index (+ 1 state.order-index))
                            (tset state :who (. state.order (+ 1 state.order-index))))))
                (do
                  (var goto-end false)
                  (set state.order-index (+ state.order-index 1))               
                  (when (> state.order-index 4)
                    (set state.order-index 1)
                    (set goto-end true))
                  (set state.who (. state.order state.order-index))
                  (if goto-end
                      (do
                        (tset state :state :end)
                        (tset state :player-action :Next)
                        )
                      (do
                        (tset state :state :play)
                        (tset state :player-action :Play)
                        )                   
                      ))
                )
            )
            
    :Counter (do
               (var next-counter (+ state.counter-index 1))
               (when (> next-counter 4)
                 (set next-counter 1))               
               (table.insert
                counters
                (fn []
                  (pp [:counter-card who (. (get-who-cards who) 1)])
                  (local card (. (get-who-cards who) 1))
                  (when card
                    (as.counter (or (. convert who) who)
                                card)))) 
               (tset state :counter-index next-counter)
               (tset state :who (. state.order state.counter-index))
               (when (= state.counter-point next-counter)
                   ;; Resolve
                   (do
                     (each [_ c (ipairs counters)] (c))
                     (set counters [])
                     (tset state :resolve [(as.resolve)])
                     (pp state.resolve)
                     (local dead (not (as.alive)))
                     (local success (and (. state.resolve 1) (. state.resolve 1) 2))
                     (if success
                         (sounds.cash)
                         dead
                         (sounds.scratch)
                         (sounds.land))
                     (when success                       
                       (let [effects (. state.resolve 2 :effects)]
                         (each [i {:type t :effect [who what amount]} (ipairs effects)]
                           (when (= :strata t)
                             (tset (. state :highlight-value
                                      (. unconvert who))
                                   what
                                   (if (> amount 0)
                                       [0.1 0.9 0.1 4]
                                       [0.9 0.1 0.1 4]
                                       ))))))
                     (tset state :who :player)
                     (tset state :state :resolve)
                     (tset state :player-action :Resolve))
               ))
    :Resolve (do
               (unselect-all)
               (var goto-end false)
               (var dead (not (as.alive)))
               (set state.order-index (+ state.order-index 1))               
               (when (> state.order-index 4)
                   (set state.order-index 1)
                   (set goto-end true))
               (set state.who (. state.order state.order-index))
               (if (or goto-end dead)
                   (do
                     (tset state :state :end)
                     (tset state :player-action :Next)
                     )
                   (do
                     (tset state :state :play)
                     (tset state :player-action :Play)
                     )                   
                   )
               
                 ;; return to play if order-index < 4 else goto End
                 
                 )
    :Next (do (tset state :state :start)
              ;; (as.set-hands {:prol [] :mill [] :olig []})
              )
    ))

(fn mute []
  (local keybindings (require :keybindings))
  (keybindings  :m))

(fn fullscreen []
  (local keybindings (require :keybindings))
  (keybindings  :f10))

(fn menu []
  (local game-modes (require :lib.game-modes))
  (fade.set-color :light-blue)
  (fade.timed-fade-out 0.5
                       (fn [] (game-modes.switch (require :mode-opening)))))

(fn love.handlers.ui-clicked [what ...]
  (sounds.click)
  (match what
    :menu (menu)
    :mute (mute)
    :full (fullscreen)
    :military (pp "mill")
    :ivy  (pp "ivy")
    :oligarch (pp "olig")
    :prol (pp "prol")
    :jack (pp "jack")
    :love (pp "love")
    :power (pp "power")
    :content (pp "content")
    :action (action ...)))
