(local repl (require :lib.stdio))
(local pallet (require :pallet))
(local anchor (require :lib.anchor))
(local game-modes (require :lib.game-modes))

(local {: title-font : text-font} (require :fonts))
(local {: sounds} (require :sounds))
(local button (require :button))
(local icons (require :icons))
(local keybindings (require :keybindings))

(local ivy (icons :ivy 128))

(var clicked false)
(var over false)
(var hover-index 1)

(local fade (require :fade))

(local state (require :state))

(fn draw []  
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  (anchor.push-anchor 0 0 w h :center :center)
  (anchor.push-column 0 0 960 400 :center 10)
    
  (anchor.push-element 0 0 960 70)  
  (love.graphics.setColor pallet.black)
  (love.graphics.setFont title-font)
  (love.graphics.printf "Axion Scion" 0 0 960 :center)
  (anchor.pop)
  (anchor.push-element 0 0 960 160)  
  (love.graphics.setColor pallet.black)

  (love.graphics.draw ivy (/ (- 960 128) 2))
  (anchor.pop)
  (fn play-game [x]
    (if state.story-played
        (button x "Play Game"
                (fn []
                  (fade.set-color :shadow)
                  (fade.timed-fade-out 0.5
                                       (fn [] (game-modes.switch (require :mode-game))))) mx my clicked)
        (button x "Play Game" (fn []
                                
                                (fade.timed-fade-out 0.5
                                                     (fn [] (game-modes.switch (require :mode-story))))) mx my clicked))
    )
  (local [o who]
         (-> [false nil]
             play-game             
             (button "Index"
                     (fn []
                       (fade.timed-fade-out
                        0.5
                        (fn [] (game-modes.switch (require :mode-index)))))
                     mx my clicked)
             (button "Credits"
                     (fn []
                       (fade.timed-fade-out
                        0.5
                        (fn [] (game-modes.switch (require :mode-credits)))))
                     mx my clicked)
             (button "Quit"
                     (fn []
                       (fade.timed-fade-out
                        0.1
                        (fn [] (love.event.quit))))
                     mx my clicked)))

  (when (~= o over)
    (set over o)
    (when over
      (let [opts [sounds.ding1 sounds.ding2 sounds.ding3 sounds.ding4
                  sounds.ding5 sounds.ding6 sounds.ding7 sounds.ding8
                  sounds.ding9 sounds.ding10 sounds.ding11 sounds.ding12]
            sound (. opts hover-index)]
        (set hover-index (+ 1 hover-index))
        (when (> hover-index 12)
          (set hover-index 1))
        (sound)
        
        )))
  (anchor.pop)
  (anchor.pop)
  (fade.draw w h)
  (set clicked false))

(fn update [self dt]
  (fade.update dt))

(fn enter [self from arg]
  (love.graphics.setBackgroundColor pallet.light-blue)
  (when (not arg)
    (if (and from from.name (= :mode-game from.name))
        (fade.set-color :shadow)
        (fade.set-color :light-blue)))
  (fade.timed-fade-in 0.5))

(fn keypressed [self key]
  (match key
    _ (keybindings key)))

(fn mousepressed [x y button] (set clicked true))

{: draw : update : mousepressed : keypressed : enter}
