(local description {})

(set description.pirate-attack "So, turns out your military is worse than useless. Let's just say that no one is happy about this.")
(set description.fly-by "When negotiations fail, just fly your biggest baddest space craft by their windows.")
(set description.centralize-power "Take a more active role in ruling your space dictatorship. Your Oligarchs will not like you encroaching on their fiefdoms.")
(set description.hostage "The Prols have become discontent and will attempt to take your Scion hostage. Your hostage negotiators are terrible, so these things never ends well. Note to self, counter this card, and get better hostage negotiators.")
(set description.power-play "The Oligarchs are discontent with your rule. They are bound to make a power play and oust you and your head. Counter this card if you wish to stay alive.")
(set description.soft-coup "The military has become discontent. They will try to smother you with pillows if you are not careful. Counter this card if you wish to wake in the morning.")
(set description.aisle-crosser "A member of your \"Parliament\" has seduced your scion. I'm sure this will end well...")
(set description.conscription "Conscription. The military loves it, but the Prols hate it. I wonder why?")
(set description.exercises "The military has sent your Scion out on an unnecessary exercise. The chance of death is high. Counter this card if you can!")
(set description.hard-coup "The time of the pillow is over, the time of the rock and gun and tank has begun. Your military is very discontent, they will try and crush your rule through force. Counter this unless you wish to see tanks rolling through your bedroom.")
(set description.meteor "This benign meteor will make great PR for your military, if they manage to shoot it down.")
(set description.service "Your scion has begun their military service.")
(set description.tax-loopholes "What is big and round and makes the rich richer? Tax Loopholes of course.")
(set description.support-union "What is a Prol without a union? No don't answer that question, clearly all unions are supported!")
(set description.market-booms "With speculation at an all time high, the rich just keep getting richer!")
(set description.cancel-holiday "Holiday? More like work all day? Am I right fellow Oligarchs.")
(set description.revolt "Well, it looks like you may have cancelled one too many holidays. The peasants are at your gates with pitchforks. Counter this unless you want to end up a smouldering despot sandwich.")
(set description.strike "When negotiations fail, and you've got the support of the union, give em a good ol' strike!")
(set description.revolution "One union bust too many leads to an unhealthy dose of revolution. Counter this unless you want to wake up at the bottom of a well.")
(set description.civic-holiday "The populace deserves a holiday! As despot will you give it to them?")
(set description.graduation "Your Scion has graduated from university. This will lower the influence of the Prols and raise the contentment of the Oligarchs.")
(set description.cozy-foxhole "While on deployment your Scion made a close friend while under fire in a cozy foxhole. This will surly end well...")
(set description.lobby "When governance has failed, why not lobby?")
(set description.cut-spending "Who needs new fighters? What's soldiers pay?")
(set description.fission-fiasco "Well, no one said your military was competent. Perhaps you should stop the Sunday beheadings?")
(set description.bust-union "What is a Prol without a union? A more efficient tool of capitalism of course!")
(set description.planet-cleared "Wasn't me....")
(set description.university "Your Scion goes to collage.")
(set description.rally-support "In case of popular opposition, just add two doses of populism one dose of media spin and a half pint of pump up music.")
(set description.new-friends "There is a rising party among the Oligarchs. It seems like your Scion has fallen in with them. Is this party a front for violent political machinations. Counter this card unless you want to find out.")
(set description.deadly-gala "The Oligarchs have had enough of your despotic ways. When the first ingredient in the punch is cyanide you know its going to be a rager. Counter this unless you want to know what if feels like to melt from the inside out.")
(set description.elected "")
(set description.promote-general "After each beheading a new General must rise!")
(set description.romance "")
(set description.devolve-power "Spread power out among the Oligarchs. I'm sure this will end well.")
(set description.military-parade "What's the best cover for incompetence? Promotion, time to promote the shit out of this Strata.")

(set description.Despot "Your hand. You can hold up to 4 cards. Try and always have a counter.")
(set description.Scion "Location of your Scion. Raises chance multiplier for host.")
(set description.Prol "The hand and stats for the Prol. Beware of intervention cards.")
(set description.Oligarch "The hand and stats for the Oligarchs. Beware of intervention cards.")
(set description.Military "The hand and stats for the Military. Beware of intervention cards.")
(set description.Content "The contentment level. Low levels lead to interventions.")
(set description.Power "The power level. Be careful of discontent powerful Strata.")

(set description.Select "Choose cards for your hand from the pot. You can hold up to 4.")

(set description.Play "Select a card to play it. You cannot play counters. Leave unselected to skip.")

(set description.Counter "Play a counter, Optional. Watch out for red cards! Leave unselected to skip.")

(set description.Draw "Move onto the next step. Raise the Event window and draw 5 cards.")

(set description.Resolve "Resolve the turn and move onto the next player. Effects will be applied!")

(set description.Oligarchcounter "Oligarch is choosing a card to counter with.")

(set description.Oligarchplay "Oligarch is choosing a card to play.")

(set description.Prolcounter "Prol is choosing a card to counter with.")
(set description.Prolplay "Prol is choosing a card to play.")

(set description.Militarycounter "Military is choosing a card to counter with.")
(set description.Militaryplay "Military is choosing a card to play.")



description
