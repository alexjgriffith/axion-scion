(fn member [ar st]
  (var ret false)
  (each [_ s (ipairs ar)]
    (when (= st s)
      (set ret true)))
  ret)

(local state (require :state))

(fn selected [where card]
  (when where    
    (match where
      :pot (member state.pot-selected card)
      :player (member state.player-selected card)
      :oligarch (member state.oligarch-selected card)
      :military (member state.military-selected card)
      :prol (member state.prol-selected card))
  ))

(local convert {:military :mill :oligarch :olig :prol :prol})

(local unconvert {:mill :military :olig :oligarch :prol :prol
                  :ivy :Despot :jack :Scion})

(fn cammel-to-capital [st]
  (let [split (lume.split st "-")]
    (table.concat
     (icollect [_ s (ipairs split)]
       (let [first (string.sub s 1 1)
             rest (string.sub s 2)]
         (.. (string.upper first) rest)
         )
      )
     " ")))


{: member : selected : convert : unconvert : cammel-to-capital}

