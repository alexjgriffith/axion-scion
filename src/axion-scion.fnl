(local axion-scion {})

(var turn 1)

(var order [:player :mill :olig :prol])

(var whos-playing :player)

(var play-m 1)

(fn player-to-index [who]
  (var index 1)
  (each [i v (ipairs order)]
    (when (= v who)
      (set index i)))
  index)

(fn next-player [who]
  (var i (player-to-index who))
  (set i (+ i 1))
  (when (> i 4)
    (set i 1))
  (set whos-playing (. order i)))

(local max-turns 12)

(local player-default {:max-cards 4
                       :cards []
                       :chance-mult 1
                       :alive true})

(fn generate-player []
  (local player (lume.clone player-default))
  (set player.cards [])
  player)

(var player (generate-player))

(local strata-default
       {:mill {:name :mill
               :max-cards 2
               :cards []
               :content 70
               :power 33
               :chance-mult 1
               :content-mult 1
               :power-mult 1
               :has-scion false
               :scion-loves false}
        :olig {:name :olig
               :max-cards 2
               :cards []
               :content 70
               :power 33
               :chance-mult 1
               :content-mult 1
               :power-mult 1                      
               :has-scion false
               :scion-loves false}
        :prol {:name :prol
               :max-cards 2
               :cards []
               :content 70
               :power 33
               :chance-mult 1
               :content-mult 1
               :power-mult 1                      
               :has-scion false
               :scion-loves false}})

(fn generate-strata []
  (local clone lume.clone)
  (local strata {:mill (clone strata-default.mill)
                 :olig (clone strata-default.olig)
                 :prol (clone strata-default.prol)})
  (set strata.mill.cards [])
  (set strata.olig.cards [])
  (set strata.prol.cards [])
  strata)

(var strata (generate-strata))

(local cards (require :cards))

(fn card-by-name [name]
  (or (. cards name) false))

(fn card-to-name [card]
  (if (= (type card) :table)
      (or (. card :name) :missing-name)
      false))

(local scion-default {:where false
                     :loves false
                     :alive true})

(var scion (lume.clone scion-default))

(var event-card false)

(var active-card false)
(var counter-cards {})

(var pot [])

(fn normalize-power []
  (let [total-power (lume.reduce strata (fn [acc v] (+ v.power acc)) 0)]
    (each [n s (pairs strata)]
      (tset s :power (math.floor (* 100 (/ s.power total-power))))))
  strata)

(fn roll-dice [card m]
  (local chance (or card.chance 1))
  (local r (math.random))
  (values (< r (* m chance)) r (* m chance) card))

(fn update-strata-scion []
  (let [{: loves : where} scion]
      (each [n s (pairs strata)]
        (if (= n loves)
            (tset (. strata n) :scion-loves true)
            (tset (. strata n) :scion-loves false))
        
        (if (= n where)
            (tset (. strata n) :has-scion true)
            (tset (. strata n) :has-scion false))
        )))

(fn get-card [card]
  (match (type card)
    :table card
    :string (card-by-name card)
    _ (error "expected a card string or obj")))

(fn apply [card ?m]
  (fn type-select [previous value]
    (match (type value)
      :bool value
      :number (+ previous value)))
  (fn scionf [[index value]]
    (tset scion index value)
    (update-strata-scion)
    [:scion index value])
  (fn playerf [[index value]]    
    (tset player index (type-select player.index value))
    [:player index value])
  (fn strataf [[s index value]]
    (when  (or (= s :olig) (= s :mill) (= s :prol))
      (local valuep
             (match index
               :power (* value (. strata s :power-mult))
               :content (* value (. strata s :content-mult))
               _ value))
      (tset (. strata s) index (type-select (. strata s index) valuep))
      (when (= index :power) (normalize-power)))
    [s index value])
  ;; {:type scion :effect [:loves :olig]}
  (let [(s r c) (roll-dice card (or ?m 1))]
    (values
     s
     r
     c
     card
     (when s
      (icollect [index {:type t : effect} (ipairs card.effects)]
        (match t
          :scion (scionf effect)
          :player (playerf effect)
          :strata (strataf effect)))))))

(fn event []
  (let [possible-cards []]
    (each [k card (pairs cards)]
      (when (and card.event (= card.event turn))
        (table.insert possible-cards card)))    
    (let [l (# possible-cards)]
      (if (> l 0)
          (set event-card (. possible-cards (math.random 1 l)))
          (set event-card false))))
  event-card)

(fn order-players []
  (let [sorted (-> strata
                 (lume.map (fn [s] [s.name s.power]))
                 (lume.invert)
                 (lume.keys)
                 (lume.sort (fn [[name power] [_ power2]] (> power power2)))
                 (lume.map (fn [[name]] name)))
      player ["player"]]
  (each [index name (ipairs sorted)]
    (table.insert player name))
  player))

(fn start []  
  ;; reset multipliers
  (each [_ s (pairs strata)]
    (each [_ k (ipairs [:chance-mult :power-mult :content-mult])]
      (tset s k 1)))
  (apply event-card)
  (set order (order-players))
  ;; set scion multiplyers
  (when scion.alive
    (apply {:effects [{:type :strata :effect [scion.where :power-mult 0.5]}
                      {:type :strata :effect [scion.loves :chance-mult 0.5]}]}))
  (values event scion))


(fn draw [number]
  (fn to-itable [t]
    (let [o []]
      (each [_ v (pairs t)]
        (table.insert o v))
      o))
  (fn filter-non-event [t]
    (let [o []]
      (each [i v (ipairs t)]
        (when (not v.event)
          (table.insert o v)))
      o))
  (fn filter-scion [t]
    (let [o []]
      (each [i v (ipairs t)]
        (when (not (or (and v.unlove (not scion.loves))
                       (and (= :scion v.type) (~= scion.where v.strata))))
          (table.insert o v)))
      o))
  (fn filter-strata [t s]
    (let [o []]
      (each [i v (ipairs t)]
        (if (~= s v.strata)
            (table.insert o v)
            (let [{: content : power} (. strata s)]
              (when (and (<= content (or v.content 100))
                        (>= power (or v.power 0)))
                (table.insert o v)))))
      o))
  (fn filter-hands [t s]
    (let [o []
          hand-names {}]
      (each [_ who (ipairs [player strata.mill strata.olig strata.prol])]
        (each [i c (ipairs who.cards)]
          (tset hand-names c.name true)))
      (each [i v (ipairs t)]
        (when (not (. hand-names v.name))
          (table.insert o v)
            ))
      o))
  (let [filtered-cards
        (-> cards
            to-itable
            filter-non-event
            filter-scion
            (filter-strata :mill)
            (filter-strata :olig)
            (filter-strata :prol)
            filter-hands
            )]
    (fn draw-card [filtered-cards]
      (var total-weight 0)
      (var sum-weight 0)
      (var card-drawn false)
      (var remove false)
      (each [index card (ipairs filtered-cards)]
        (set total-weight (+ total-weight card.weight)))
      (let [r (math.random total-weight)
            filtered-cards-removed []]
        (each [index card (ipairs filtered-cards)]
          (if (and (> r sum-weight) (<= r (+ sum-weight card.weight)))
              (set card-drawn card)
              (table.insert filtered-cards-removed card))
          (set sum-weight (+ sum-weight card.weight)))
        (values card-drawn filtered-cards-removed)))
    (var new-filtered filtered-cards)
    (let [cards-drawn []]
      (for [i 1 (or number 1)]
        (let [(card-to-add next-filtered) (draw-card new-filtered)]
          (set new-filtered next-filtered)
          (table.insert cards-drawn card-to-add)))
      (set pot cards-drawn))
    ;; (pp cards-drawn)
    pot))

(fn swap-card [card from to]
  (let [from-ret []
        to-ret (lume.clone to)]
    (each [_ c (ipairs from)]
      (if (= c card)
          (table.insert to-ret c)
          (table.insert from-ret c)))
    (values from-ret to-ret)))

(fn get-entity [who]
  (match who
    :player player
    :mill (. strata :mill )
    :olig (. strata :olig )
    :prol (. strata :prol )))
  
(fn select-card [who card-st]
  (local card (get-card card-st))  
  (local ent (get-entity who))
  (set (pot ent.cards) (swap-card card pot ent.cards))
  (values ent.cards pot))

(fn return-card [who card-st]
  (local card (get-card card-st))  
  (local ent (get-entity who))
  (set (ent.cards pot) (swap-card card ent.cards pot))
  (values ent.cards pot))

(fn select-return-cards [who cards-selected ?cards-returned]
  (when ?cards-returned
    (each [_ card (ipairs ?cards-returned)]      
      (return-card who card)))
  (each [_ card (ipairs cards-selected)]      
    (select-card who card))
  (local ent (get-entity who))
  (values ent.cards pot))

(fn clear-pot []
  (set pot []))

(fn play [who card-st]
  (local card (get-card card-st))
  ;; (local who whos-playing)  
  (local active-card-table [])
  (local ref (match who
               :player player
               :mill strata.mill
               :olig strata.olig
               :prol strata.prol
               ))
  (set play-m (or ref.chance-mult 1))
  (set ref.cards
       (swap-card card ref.cards active-card-table))
  (set active-card card)
  ;; (next-player who)
  (values card ref.cards))

(fn counter [who counter-card-st]
  (local counter-card (get-card counter-card-st))
  (local counter-card-table [])
  (local ref (match who
               :player player
               :mill strata.mill
               :olig strata.olig
               :prol strata.prol
               ))
  (set ref.cards
        (swap-card counter-card ref.cards counter-card-table))
  (tset counter-cards who counter-card)
  (values counter-card ref.cards))

(fn resolve []
  (let [success {}]
    (var any-success false)
    (each [_ name (ipairs order)]      
      (let [counter-card (. counter-cards name)
            m (match name
                :player player.chance-mult
                :olig strata.olig.chance-mult
                :mill strata.mill.chance-mult
                :prol strata.prol.chance-mult
                _ 1
                     )]
        (when counter-card
          (let [(s r c) (roll-dice counter-card m)]
            (tset success name [s r c])
            (when s
              (set any-success true))))))
    (local outcome
           (when (= false any-success)
             [(apply active-card play-m)]))
    (var old-active-card active-card)
    (set active-card false)
    (set counter-cards {})
    (values outcome old-active-card success)))

(fn end []
  (set turn (+ turn 1))
  (set strata.olig.cards [])
  (set strata.mill.cards [])
  (set strata.prol.cards [])
  (not (and player.alive scion.alive)))

(fn get-event-card []
  event-card)

(fn get-strata-hand [s]
  (. strata s :cards))

(fn get-strata-stats [s]
  (. strata s))

(fn get-player-hand []
  (. player :cards))

(fn get-scion-card []
  {:name :scion
   :type :character
   :icon :jack
   :where scion.where
   :loves scion.loves})

(fn get-pot []
  pot)

(fn get-active-card []
  active-card)

(fn get-counter-cards []
  counter-cards)

(fn get-turn []
  turn)

(fn reset-state []
  (set pot [])
  (set active-card false)
  (set counter-cards [])
  (set scion (lume.clone scion-default))
  (set strata (generate-strata))
  (set player (generate-player))
  (set order [:player :mill :olig :prol])
  (set whos-playing :player)
  (set turn 1))

(fn get-hand [who hand]
  (let [hand (match who
               :pot  pot                
               :player player.cards               
               :mill strata.mill.cards
               :olig strata.olig.cards
               :prol strata.prol.cards
               :active [active-card]
               :event [event-card]
               :counter (icollect [_ c (pairs counter-cards)] c)
               )]    
    (icollect [_ v (ipairs hand)] (card-to-name v))))

(fn set-hand [who hand]
  (match who
    :pot (do (set pot (icollect [_ v (ipairs hand)] (card-by-name v)))
             pot)
    :player (do
              (set player.cards
                   (icollect [_ v (ipairs hand)] (card-by-name v)))
              player.cards)
    :mill (do (set strata.mill.cards
                   (icollect [_ v (ipairs hand)] (card-by-name v)))
              strata.mill.cards)
    :olig (do (set strata.olig.cards
                   (icollect [_ v (ipairs hand)] (card-by-name v)))
              strata.olig.cards)
    :prol (do (set strata.prol.cards
                   (icollect [_ v (ipairs hand)] (card-by-name v)))
              strata.prol-cards)))

(fn set-hands [st]
  (each [who hand (pairs st)] (set-hand who hand))
  {: pot
   :player player.cards
   :mill strata.mill.cards
   :olig strata.olig.cards
   :prol strata.prol.cards})

(fn set-state [st]
  (each [key value (pairs st)]
    :player (do (set player (lume.merge player value))
                (when value.cards (set-hand :player value.cards)))
    :mill   (do (set strata.mill (lume.merge strata.mill value))
                (when value.cards (set-hand :mill value.cards)))
    :olig   (do (set strata.olig (lume.merge strata.olig value))
                (when value.cards (set-hand :olig value.cards)))
    :prol   (do (set strata.prol (lume.merge strata.prol value))
                (when value.cards (set-hand :prol value.cards)))
    :scion  (do (set scion (lume.merge scion value))
                (update-strata-scion))
    :event  (set event-card (or (card-by-name value) false))
    :active (set active-card (or (card-by-name value) false))
    :counter (do
               (set counter-cards {})
               (each [key v (ipairs value)]
                 (tset counter-cards key (card-by-name value))))
    :turn (set turn value)
    :order (set order value)
    :who (set whos-playing value)))

{: reset-state
 : event
 : start
 : draw
 : return-card
 : select-card
 : select-return-cards
 : clear-pot
 : play
 : counter
 : resolve
 : end
 : get-turn
 : get-hand
 : set-hand
 : set-hands
 : set-state
 : get-event-card
 : get-strata-hand
 : get-strata-stats
 : get-player-hand
 : get-scion-card
 : get-pot
 : get-active-card
 : get-counter-cards
 :alive (fn [] (values
                 (and player.alive scion.alive)
                 player.alive scion.alive))
 :order order-players
 }
