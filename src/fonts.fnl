(local big-font (love.graphics.newFont "assets/fonts/EdgeOfTheGalaxy.otf" 128))

(local title-font (love.graphics.newFont "assets/fonts/EdgeOfTheGalaxy.otf" 64))
(local text-font (love.graphics.newFont "assets/fonts/SourceCodePro.otf" 32))

(local display (love.graphics.newFont "assets/fonts/SourceCodePro-Bold.otf" 32))

(local button-font
       (love.graphics.newFont "assets/fonts/SourceCodePro-Bold.otf" 32))

(local card-button-font
       (love.graphics.newFont "assets/fonts/SourceCodePro-Bold.otf" 20))

(local card-title-font
       (love.graphics.newFont "assets/fonts/SourceCodePro-Bold.otf" 14))

(local card-chance-font
       (love.graphics.newFont "assets/fonts/SourceCodePro-It.otf" 16))

(local card-type-font
       (love.graphics.newFont "assets/fonts/SourceCodePro-It.otf" 12))

(local outcome-heading-font
       (love.graphics.newFont "assets/fonts/SourceCodePro-Bold.otf" 32))

(local outcome-font
       (love.graphics.newFont "assets/fonts/SourceCodePro.otf" 24))

{: title-font : text-font : card-title-font : card-type-font : card-chance-font
 : card-button-font : outcome-heading-font : outcome-font : button-font : big-font : display}
