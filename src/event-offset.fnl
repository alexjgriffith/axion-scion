(local flux (require :lib.flux))

(local event-offset {:y -720
                     :fg (flux.group)})

(fn event-offset.drop [period callback]  
  (local tween (event-offset.fg:to event-offset period {:y 0}))
  (tween:ease :quadout)
  (when callback
    (tween:oncomplete callback)))

(fn event-offset.raise [period callback]
  (local tween (event-offset.fg:to event-offset period {:y -1000}))
  (tween:ease :quadin)
    (when callback
      (tween:oncomplete callback)))

(fn event-offset.update [dt]
  (event-offset.fg:update dt))

event-offset
