(local repl (require :lib.stdio))

(local state (require :state))

(local keybindings (require :keybindings))

(local game-modes (require :lib.game-modes))

(var re-enter false)

(var gdt 0)

(var click false)

(local fade (require :fade))

(local as (require :axion-scion))

(local states ["start"
               "draw"
               "select"
               "play"
               "end"])

(local event-offset (require :event-offset))

(local help-offset (require :help-offset))

(local {: sounds} (require :sounds))

(fn update-start []
  (local {: end-scion-text : end-player-text : end-win-text} (require :text))
  (local event-card (as.event))
  (local (alive pl sc) (as.alive))
  (if (and event-card alive)
      (do (sounds.page)
          (event-offset.drop
           0.25
           (fn [x])
           (as.start)   
           (tset state :state "draw")
           (tset state :player-action :Draw)))
      (do
        (tset state :ending-title
              (if
               (not sc) "Your Scion is Dead!"
               (not pl)
               "You've Died!"
               "Game Over (Win Condition)"
               ))
        (tset state :ending-text
              (if
               (not sc) end-scion-text
               (not pl)
               end-player-text
               end-win-text
               ))
        
        (tset state :state :done)
        (fade.set-color :light-blue)
        (fade.timed-fade-out 0.5
                             (fn []
                               (tset state :story-played false)
                               (tset state :order-index 1)
                               (tset state :who :player)
                               (tset state :state :start)
                               (as.reset-state)
                               (game-modes.switch (require :mode-ending))))))
      )

(fn update-draw [])

(fn with [callback who]
  (match who
    :oligarch (callback :oligarch :olig)
    :military (callback :military :mill)
    :prol  (callback :prol :prol)))

(local ai (require :ai))

(var time 0)
(var period 0)
(var last-state :start)
(var callback nil)

(fn timer [period-in callback-in]
  (set last-state state.state)
  (set time 0)
  (set period period-in)
  (set callback callback-in)
  (set state.ps state.state)
  (set state.state :timer))

(fn update-timer [dt]
  (set time (+ time dt))
  (when (> time period)
    (when callback (callback))
    (set state.state last-state)))

(fn update-select []
  (when (~= state.who :player)
    (timer 1 (fn [] (with ai.select state.who)))))

(fn update-play []
  (when (~= state.who :player) 
    (timer 1 (fn [] (with ai.play state.who)))))

(fn update-counter []  
  (when (~= state.who :player)
    (timer 1 (fn [] (with ai.counter state.who)))))

(fn update-resolve [])

(fn update-end []
  (tset state :state :start)
  (pp (as.get-turn))
  (pp state.turn)
  (as.end)
  (tset state :turn (+ state.turn 1)))

(fn enter [self]  
  (local pallet (require :pallet))
  (love.graphics.setBackgroundColor pallet.shadow)
  ;; (fade.set-color :shadow)
  (fade.timed-fade-in 1)
  )


(fn update [self dt]
  (when state.re-enter
    (set state.re-enter false)
    (enter))
  (match state.state
    :start (update-start)
    :draw (update-draw)
    :select (update-select)
    :play (update-play)
    :counter (update-counter)
    :resolve (update-resolve)
    :timer (update-timer dt)
    :end (update-end))
  (each [c p (pairs state.highlight-value)]
    (each [o t (pairs p)]
      (tset t 4 (- (. t 4) dt))
      ))
  (event-offset.update dt)
  (help-offset.update dt)
  (fade.update dt)
  (set gdt dt))

(fn outcome [mx my click]
  (when (= state.state :resolve)
    (local {: cammel-to-capital : unconvert} (require :utils))
    (local card (. state.resolve 2))
    (local success (and (. state.resolve 1) (. state.resolve 1 1)))
    (local blocks (. state.resolve 3))
    ;; (tset pallet.shadow 4 1)
    ;; (love.graphics.setColor pallet.light-blue)      
    ;; (love.graphics.rectangle :fill 100 100 (- 960 200) (- 720 200) 10)
    ;; (love.graphics.setColor pallet.shadow)
    ;; (love.graphics.rectangle :line 110 110 (- 960 220) (- 720 220) 10)
    (local font (require :fonts))
    (love.graphics.setColor pallet.light-blue)
    (love.graphics.setFont font.outcome-heading-font)        
    (love.graphics.printf (if success
                              :Success
                              :Failure ) 100 180 (- 960 200) :center)
    
    (love.graphics.setFont font.outcome-font)
    (love.graphics.setColor pallet.hot-pink)
    (var block-y 210)
    (when blocks
      (each [who b (pairs blocks)]
          (love.graphics.printf
           (string.format "%s counter - %s" (cammel-to-capital
                                             (or (. unconvert who) who)) (if (. b 1)
                                                       :Succeeded
                                                       :Failed))
           100 block-y (- 960 200) :center)
          (set block-y (+ block-y 20))
          ))
    (love.graphics.setColor pallet.shadow)
    (local {: card-draw : card-width : card-height} (require :render-card))
    (local margin 10)
    (local init-x (/ (- 960 card-width) 2))
    (local init-y (/ (- 720 card-height) 2))
    (local {: light-blue} (require :pallet))    
    (love.graphics.rectangle :fill
                             (- init-x (* 0.5 margin))
                             (- init-y (* margin 0.5))
                             (+ card-width (* 1 margin))
                             (+ card-height (* 1 margin))
                             10)
    (love.graphics.setColor light-blue)
    (when card
      (card-draw (. card :name) 0 0 false init-x init-y)      
      )
    ))

(fn event []
  
    (local lg love.graphics)
    (local font (require :fonts))
    (local {: cammel-to-capital : unconvert} (require :utils))
    (local e (as.get-event-card))
    (when e
    ;; (pp e)
      (local oy (+ event-offset.y))
      (tset pallet.shadow 4 1)
      (lg.setColor pallet.light-blue)      
      (lg.rectangle :fill 100 (+ 100 oy) (- 960 200) (- 720 200) 10)
      (lg.setColor pallet.shadow)
      (lg.rectangle :line 100 (+ 100 oy) (- 960 200) (- 720 200) 10)
      (lg.rectangle :line 110 (+ 110 oy) (- 960 220) (- 720 220) 10)
      (lg.setColor pallet.shadow)
      (lg.setFont font.outcome-heading-font)
      (lg.printf e.title 110 (+ 120 oy)  (- 960 220) :center)
      (local icons (require :icons))
      (lg.draw (icons e.icon 32) (/ (- 960 32) 2) (+ 160 oy))
      (lg.setFont font.outcome-font)
      (lg.printf e.text 120 (+ 200 oy)  (- 960 240) :left)
      (local {: card-draw : card-width : card-height} (require :render-card))

      (local init-x (/ (- 960 card-width) 2))
      (local init-y (+ oy (/ (- 720 card-height) 2)))

    
      ;; (card-draw (. e :name) 0 0 false init-x (+ 100 init-y))
    )
    
    
  )

(local anchor (require :lib.anchor))

(fn fade-in [])

(fn hud [focus]
  (local {: unconvert : cammel-to-capital} (require :utils))
  (local description (require :description))
  (let [[what who] focus]    
    ;; (pp help-offset)    
    (var name (cammel-to-capital (or (. unconvert what) what "")))    
    (when (= :Action name)
      (set name state.player-action)
      ;; (pp name)
      )
    ;; (pp name)
    (if (and state.help what (. description name))
        (help-offset.raise 0.05)
        (help-offset.drop 0.5))
      (love.graphics.setColor 1 1 1 1)
      (anchor.push-anchor 0 0 960 720 :bottom)
      (anchor.push-element 0 help-offset.y  400 165)
      (love.graphics.setColor pallet.light-blue)
      (love.graphics.rectangle :fill -10 -10 420 150 10)
      (love.graphics.setColor pallet.shadow)
      (love.graphics.rectangle :line 0 0 400 130 10)
      (love.graphics.printf
       (or (. description name) name )
                            20 20 360 :left)
      (anchor.pop)
      (anchor.pop)    
      ))

(fn draw [self]
  (local scale 1)
  (local (w h) (love.window.getMode))
  (local (ox oy) (values (math.floor (/ (- w (* scale 960)) 2))
                         (math.floor (/ (- h (* scale 720)) 2))
                         ))
  (love.graphics.push "all")
  (love.graphics.translate ox oy)
  (love.graphics.scale scale)
  (let [(mxp myp) (love.mouse.getPosition)]    
    (local mx (/ (-  mxp ox) scale))
    (local my (/ (- myp oy) scale))
    (local sample-card (require :sample-card))
    (var focus (sample-card gdt mx my click))    
    (outcome mx my click)
    (event mx my click)
    (when (~= state.who :player)
      (set focus [(string.format "%s%s" state.who state.ps )])
      (pp focus)
      )
    (hud focus))
  (set click false)
  (love.graphics.pop)
  (fade.draw w h))

(fn mousepressed [self _x _y button]
  (set click button))

(fn keypressed [self key]
  (match key
    _ (keybindings key)))

(fn leave [self ...]
  ;; (pp [self ...])
  ;; (local pallet (require :pallet))
  ;; (love.graphics.setBackgroundColor pallet.light-blue)  
  ;;(fade.timed-fade-in 1)
  )

{: draw : update : keypressed : mousepressed : enter : leave}
