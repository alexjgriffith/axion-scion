(local flux (require :lib.flux))

(local help-offset {:y 1000
                    :fg (flux.group)})

(fn help-offset.drop [period callback]  
  (local tween (help-offset.fg:to help-offset period {:y 1000}))
  ;; (tween:ease :quadin)
  (when callback
    (tween:oncomplete callback)))

(fn help-offset.raise [period callback]
  (local tween (help-offset.fg:to help-offset period {:y 0}))
  ;; (tween:ease :quadout)
  (when callback
    (tween:oncomplete callback)))

(fn help-offset.update [dt]
  (help-offset.fg:update dt))

help-offset
