{:bgm nil
 :web false
 :dev false
 :sfx {}
 :mute false
 :turn 1
 :resolve nil
 :highlight-value {:prol {:power [1 1 1 0]
                          :content [1 1 1 0]}
                   :military {:power [1 1 1 0]
                              :content [1 1 1 0]}
                   :oligarch {:power [1 1 1 0]
                              :content [1 0 0 1]}}
 :commited false
 :pot-selected []
 :player-selected []
 :oligarch-selected []
 :military-selected []
 :prol-selected []
 :state :start
 :ps :start 
 :player-action :next
 :counter-active :player
 :who :player
 :order [:player :military :oligarch :prol]
 :order-index 1
 :counter-point 1
 :counter-index 1
 :play-active :player
 :story-played false
 :ending-title "Game Over"
 :ending-text "You managed to escape to your paradise world!"
 :card-name nil
 :re-enter false
 :help true}
