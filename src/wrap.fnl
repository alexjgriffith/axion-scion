(local repl (require :lib.stdio))
(local gamestate (require :lib.game-modes))

(local state (require :state))
(global pallet (require :pallet))

;; make sure none of the card names are messed up
(local cards (require :cards))
(each [name card (pairs cards)]
  (assert (= name card.name) (.. card.name " " name)))

(fn love.load [args]  
  (love.graphics.setBackgroundColor pallet.light-blue)
  (local web (= :web (. args 1)))
  (tset state :web web)
  (require :handlers)
  (local {: sounds : bgm} (require :sounds))
  (bgm)
  (sounds.page)
  (gamestate.registerEvents)
  (gamestate.switch (require :mode-opening) :wrap)
  (when (and state.dev (not web))
      (repl.start)))
