(local anchor (require :lib.anchor))
(local pallet (require :pallet))
(local {: sounds} (require :sounds))
(local {: button-font} (require :fonts))

(fn button [parent-over text callback mx my click]
  (anchor.push-anchor 0 0 960 60 :top :top)
  (anchor.push-element 0 0 300 60)
  (local over (anchor.over mx my))
  (love.graphics.setColor (if over
                              (do                                
                                (when click
                                  (sounds.page)
                                  (callback))
                                pallet.skin)
                              pallet.hot-pink))
  (love.graphics.rectangle :fill 0 0 300 60 10)
  ;;(love.graphics.setColor pallet.shadow)
  ;;(love.graphics.rectangle :line 0 0 300 60 10)
  (love.graphics.setFont button-font)
  (love.graphics.setColor pallet.light-blue)
  (love.graphics.printf text 0 7 300 :center)  
  (anchor.pop)
  (anchor.pop)
  (if (. parent-over 1)
      parent-over
      (values [over text])))
