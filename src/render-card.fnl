(local card-scale 40)
(local card-width (* 3.5 card-scale))
(local card-height (* 4 card-scale))

(local pallet (require :pallet))
(local icons (require :icons))
(local fonts (require :fonts))
(local cards (require :cards))

(local {: member : selected : cammel-to-capital} (require :utils))

(local title-y 35)

(local type-y 50)

(local chance-y 65)

(local icon-y 5)

(local effect-y 90)

(fn draw-effect [index effect]
  (love.graphics.setColor pallet.shadow)
  (local {:type t :effect e} effect)
  (local map-e {:alive :knife :where nil :loves :heart
                :mill :military :prol :prol :olig :oligarch
                :power :fist :content :heart})
  (local (icon1 icon2 icon3)
         (match t
           :player (values :ivy (. map-e (. e 1)))
           :scion (values :jack (. map-e (. e 1)) (. map-e (. e 2)))
           :strata (values (. map-e (. e 1)) (. map-e (. e 2) ) (if (> (. e 3) 0):up :down))))
  
  (local (x y)
         (match index
           1 (values 15 effect-y)
           2 (values 80 effect-y)
           3 (values 15 (+ 30 effect-y))
           4 (values 80 (+ 30 effect-y))))
  ;; types player scion strata  
  (when x    
    (when icon1
      (love.graphics.draw (icons icon1 16) x y))
    (when icon2
      (love.graphics.draw (icons icon2 16) (+ x 16) y))
    (when icon3
      (love.graphics.draw (icons icon3 16) (+ x 28) y)))
  ;; (love.graphics.rectangle :line 5 effect-y 20 20)
  )

(fn card-draw [card-st mx my click x y where? index?]
  (if (not card-st)
      (pp (string.format "Error: missing-card: %s %s") where? index?))
  (when card-st
    (local card (. cards card-st))
    (assert (= (type card) :table) (string.format "Missing Card %s" card-st))
    (local lg love.graphics)
    (lg.push)
    (lg.translate x y)
    (local over (pointWithin mx my x y card-width card-height))
    (fn set-color []
      (lg.setColor (if over
                       pallet.skin
                       (selected where? index?)
                       pallet.light-grey
                       pallet.light-blue)))
    (set-color)
    (when (= card.type :intervention)
      (love.graphics.setColor pallet.red))
    (lg.rectangle :fill 0 0 card-width card-height 10)
    (set-color)    
    (lg.rectangle :fill 5 5 (- card-width 10) (- card-height 10) 10)
    (lg.setColor pallet.shadow)
    (lg.rectangle :line 5 5 (- card-width 10) (- card-height 10) 10)
  
    (when card.icon
      ;; (lg.setColor pallet.light-blue)
      ;; (lg.rectangle :fill 5 icon-y 32 32 10)
      (lg.setColor pallet.shadow)
      (lg.draw (icons card.icon 32) (/ (- card-width 32) 2) icon-y))
    
    (when card.strata
      (set-color)
      (lg.rectangle :fill (- card-width  24) (- card-height 24) 24 24 10)
      ;; (lg.rectangle :fill (- card-width 5 32) icon-y 32 32 10)
      (lg.setColor pallet.shadow)
      (lg.draw (icons (match card.strata
                        :mill :military
                        :olig :oligarch
                        :prol :prol) 16)
               (- card-width  20)
               (- card-height 20)))
    
  
    (when card.chance
      (lg.setColor pallet.shadow)
      (lg.setFont fonts.card-chance-font)
      (lg.printf (string.format "%d%%" (math.floor (* 100 card.chance)))
                 0 chance-y card-width :center))
    
    (lg.setFont fonts.card-title-font)
    (lg.printf (cammel-to-capital card.name) 0 title-y card-width :center)
    
    (lg.setFont fonts.card-type-font)
    (lg.printf card.type 0 type-y card-width :center)
  
  ;; (lg.setColor pallet.light-blue)
  ;; (lg.rectangle :fill 5 100 (- card-width 10) 30 5)
  ;; (lg.setColor pallet.shadow)
  ;; (lg.draw up 10 105)
  
  ;; (lg.setColor pallet.light-blue)
  ;; (lg.rectangle :fill 5 135 (- card-width 10) 30 5)
  ;; (lg.setColor pallet.shadow)
  ;; (lg.draw down 10 140)
    ;;(pp card)
    (when card.effects
      ;;(pp :draw-effects)
      (each [i c (ipairs card.effects)]
      (draw-effect i c)))
    (when (and where? over click)
      (love.event.push :card-clicked where? index? click))
    (lg.pop))
  )

{: card-draw : card-scale : card-width : card-height}
