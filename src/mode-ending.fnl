(local repl (require :lib.stdio))

(local pallet (require :pallet))

(local anchor (require :lib.anchor))

(local game-modes (require :lib.game-modes))

(local fade (require :fade))

(local {: title-font : text-font} (require :fonts))

(var clicked false)

(local {: sounds} (require :sounds))

(local state (require :state))

(local button (require :button))

(var over false)

(fn draw []
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  (anchor.push-anchor 0 0 w h :center :center)
  (anchor.push-column 0 120 960 720 :center 10)    
  (anchor.push-element 0 0 960 80)  
  (love.graphics.setColor pallet.black)
  (love.graphics.setFont title-font)
  (love.graphics.printf state.ending-title 0 0 960 :center)
  (anchor.pop)
  (anchor.push-element 0 0 960 300)  
  (love.graphics.setFont text-font)
  (love.graphics.setColor pallet.black)
  (love.graphics.printf state.ending-text 0 40 960 :center)
  (anchor.pop)
  
  (local [o who]
         (-> [false nil]
             (button "Back to Menu"
                     (fn []
                       (fade.set-color :light-blue)
                       (fade.timed-fade-out
                        0.5
                        (fn [] (game-modes.switch (require :mode-opening)))))
                     mx my clicked)))
  (when (~= o over)
    (set over o)
    (when over
      (let [opts [sounds.ding1 sounds.ding2 sounds.ding3 sounds.ding4]
            sound (. opts (math.random 1 4))]
        (sound)
        
        )))  
  (anchor.pop)
  (anchor.pop)
  (fade.draw w h)
  (set clicked false)
  )


(fn enter [self from arg]
  (love.graphics.setBackgroundColor pallet.light-blue)
  (fade.timed-fade-in 0.5))

(fn mousepressed [x y button] (set clicked true))

(fn update [self dt]
  (fade.update dt))

(local keybindings (require :keybindings))
(fn keypressed [self key]
  (match key
    _ (keybindings key)))

{: draw : update : keypressed : mousepressed : enter}
