(local state (require :state))

(local anchor (require :lib.anchor))

(local fade (require :fade))

(local {: sounds} (require :sounds))

(local button (require :button))

(local {: shadow} (require :pallet))

(local {: card-draw : card-scale
        : card-width : card-height}
       (require :render-card))

(local game-modes (require :lib.game-modes))

(local {: title-font : text-font : card-chance-font} (require :fonts))

(local {: cammel-to-capital : unconvert} (require :utils))

(var over false)
(var clicked false)

(fn enter [self from arg]
  (love.graphics.setBackgroundColor pallet.light-blue)
  ;; (fade.set-color :light-blue)
  (fade.timed-fade-in 0.5)
  (tset state :card-name arg))

(fn draw-card-details []
  (let [card (. (require :cards) state.card-name)
        fennel (require :lib.fennel)]
    ;; (love.graphics.printf (fennel.view card) 0 0 960 :left)
    (love.graphics.setFont text-font)
    (love.graphics.setColor shadow)
    (love.graphics.printf (string.format "Type: %s" (cammel-to-capital card.type)) 0 0 960 :left)
    (love.graphics.printf (string.format "Chance: %s%%" (* 100 (or card.chance 1))) 0 30 960 :left)
    (when card.strata
      (love.graphics.printf (string.format "Strata: %s" (cammel-to-capital (. unconvert card.strata))) 0 60 960 :left))
    (when card.effects
      (each [i e (ipairs card.effects)]
        (let [st (string.format "Effect: %s"
                                (match e.type
                                  :strata (let [[who what by] e.effect
                                                increase (< 0 by)]
                                            (string.format "%s %s %s"
                                                           (if increase
                                                               "Raise"
                                                               "Lower")
                                                           (cammel-to-capital who)
                                                           (cammel-to-capital what))
                                            )
                                  :scion (let [[what value] e.effect]
                                           (match what
                                             :where (string.format "Scion with %s" (. unconvert value))
                                             :loves (string.format "%s seduces Scion" (. unconvert value))
                                             :alive "Kills Scion"
                                             _ "")
                                           )
                                  :player (let [[what value] e.effect]
                                            (match what
                                              :alive "Kills Despot (You)"
                                              _ "")
                                            ) 
                                  _ ""
                                  ))]
          (love.graphics.printf st 0 (+ (* 30 i) 60) 960 :left))))
    )
  )

(fn draw [self]
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  (anchor.push-anchor 0 0 w h :center :center)
  (anchor.push-column 0 0 960 720 :center 10)
  
  (anchor.push-element 0 0 960 100)
  (love.graphics.setColor shadow)
  (love.graphics.setFont title-font)
  (love.graphics.printf (cammel-to-capital state.card-name) 0 20 960 :center)
  (anchor.pop)
  (anchor.push-row 0 0 960 220 :center 10)
  (love.graphics.setColor 1 1 1 1)
  (anchor.push-element (/ (- (/ 960 3) card-width) 2) 0 (/ 960 3) card-height)
  (when state.card-name
    (card-draw state.card-name 0 0 false 0 0))
  (anchor.pop)    
  (anchor.push-element 0 0 960 350)
  (draw-card-details)
  (anchor.pop)
  (anchor.pop) ;; row
  (anchor.push-element 0 0 960 280)
  (let [description (require :description)
        cd (. description state.card-name)]
    (when cd
      (love.graphics.setFont text-font)
      (love.graphics.setColor shadow)
      (love.graphics.printf cd 20 0 920 :left))
    )
  (anchor.pop)
  (love.graphics.setColor 1 1 1 1)
  (local [o who]
         (-> [false nil]
             (button "Back"
                     (fn []
                       (pp state.re-enter)
                       ;; (fade.set-color (match state.re-enter
                       ;;                   :game :shadow
                       ;;                   _ :light-blue))
                       (fade.timed-fade-out
                        0.5
                        (fn []
                          (pp "back")
                          (game-modes.pop)                          
                          )))
                     mx my clicked)))
  (when (~= o over)
    (set over o)
    (when over
      (let [opts [sounds.ding1 sounds.ding2 sounds.ding3 sounds.ding4]
            sound (. opts (math.random 1 4))]
        (sound)
        
        )))  
  
  (anchor.pop)
  (anchor.pop)
  (fade.draw w h)
  (set clicked false)
  )

(fn mousepressed [x y button] (set clicked true))

(fn update [self dt]
  (fade.update dt))

{: draw : update : enter : mousepressed}
