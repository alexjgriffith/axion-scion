(local {: sounds : mute : unmute} (require :sounds))
(local {: web} (require :state))

(local game-modes (require :lib.game-modes))

(var muted false)
(var fullscreen false)

(fn toggle-mute []
  (if muted
      (do (set muted false)               
          (unmute)
          (sounds.click))
      (do (set muted true)
          (mute))))

(fn toggle-fullscreen []
  (if web
      (do (JS.callJS "toggleFullScreen();")
          (set fullscreen (not fullscreen))
          (if (not fullscreen)
              (love.window.setMode 960 720)
              (love.window.setMode 0 0))
          )
      (do (local (x y flags) (love.window.getMode))  
          (tset flags :fullscreen (not flags.fullscreen))
          (love.window.setMode x y flags))))

(fn not-web [call] (when (not web) (call)))

(fn screenshot []
  (sounds.click)
  (love.graphics.captureScreenshot (.. (os.time) "-axion-scion-screenshot.png")))


(fn toggle-opening []
  (game-modes.switch (require :mode-opening)))

(fn toggle-preview []
  (game-modes.switch (require :mode-preview)))

(fn toggle-card []
  (game-modes.switch (require :mode-card)))

(fn keybindings [key]
  (match key
    :m (toggle-mute)
    :q (not-web screenshot)
    :f10 (toggle-fullscreen)
    :1 (toggle-opening)
    :2 (toggle-preview)
    :3 (toggle-card)
    ;; :enter (select-action)
    ))
