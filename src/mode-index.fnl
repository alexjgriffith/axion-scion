(local repl (require :lib.stdio))

(local pallet (require :pallet))

(local anchor (require :lib.anchor))

(local game-modes (require :lib.game-modes))

(local fade (require :fade))

(local {: title-font : text-font : card-chance-font} (require :fonts))

(var clicked false)

(var down false)

(local {: sounds} (require :sounds))

(var over false)

(var re-enter false)

(var init nil)
(var old-scroll 0)

(local button (require :button))

(local credits1 "
COMING SOON!")

(local credits2 "")

(local cards (require :cards))

(local {: card-draw : card-scale
        : card-width : card-height}
       (require :render-card))

(local canvas (love.graphics.newCanvas 900 1800))
(local quad (love.graphics.newQuad 0 0 900 460 900 1800))

(var height 0)
(var scroll 0)

(fn draw []
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  (anchor.push-anchor 0 0 w h :center :center)
  (anchor.push-column 0 60 960 720 :center 10)    
  (anchor.push-element 0 0 960 80)  
  (love.graphics.setColor pallet.shadow)
  (love.graphics.setFont title-font)
  (love.graphics.printf "Index" 0 0 960 :center)
  (anchor.pop)
  (love.graphics.setColor 1 1 1 1)
  (anchor.push-row 0 0 960 480 :center 10)
  ;; (anchor.push-column 0 0 960 460 :center 10)
  
  ;;(anchor.push-anchor 0 0 900 460 :center)
  (anchor.push-window 20 0 canvas quad)
  (anchor.push-grid 0 0 900 460 10 10)
  (var i 1)
  (set height 0)
  (local (vx vh vw vh) (quad:getViewport))
  (quad:setViewport 0 scroll vw vh)
  (each [n _ (pairs cards)]
    (anchor.push-element 0 0 card-width card-height)
    (local (col cx cy) (anchor.over mx my))
    (when (and clicked col)
      (fade.timed-fade-out
       0.5
       (fn []
         (set re-enter true)
         (game-modes.push (require :view-card) n))))
    (card-draw n cx cy false 0 0)
    (anchor.pop)
    (when (= 1 (% i 8))
      (set height (+ height card-height 10)))
    (set i (+ i 1)))
  (anchor.pop)
  (anchor.pop canvas quad)
  ;; (anchor.pop)
  (local rel (- height (* 2 card-height) -50))
  (when init
    (let [h (- 460 100)
          d (- my init)]
      (set scroll (math.min rel (math.max 0 (+ old-scroll (* rel (/ d h))))))))
  (anchor.push-element 20 (* (/ scroll rel) (- 460 100)) 10 100)
  (if (anchor.over mx my)
      (do (when clicked
            (set init (+ (+ my)))
            (set old-scroll scroll))
          (when init (pp (- my init)))
          (love.graphics.setColor pallet.skin))
      (if init
          (love.graphics.setColor pallet.skin)
          (love.graphics.setColor pallet.shadow)))  
  (love.graphics.rectangle :fill 0 0 10 100)  
  (anchor.pop)  
  (anchor.pop)
  
  (local [o who]
         (-> [false nil]
             (button "Back"
                     (fn []
                       (fade.timed-fade-out
                        0.5
                        (fn [] (game-modes.switch (require :mode-opening)))))
                     mx my clicked)))
  (when (~= o over)
    (set over o)
    (when over
      (let [opts [sounds.ding1 sounds.ding2 sounds.ding3 sounds.ding4]
            sound (. opts (math.random 1 4))]
        (sound)
        
        )))  
  (anchor.pop)
  (anchor.pop)
  (love.graphics.setColor 1 1 1 1)
  (love.graphics.setCanvas)
  (fade.draw w h)
  (set clicked false)
  )

(fn enter [self from arg]
  ;; (fade.set-color :)
  (fade.timed-fade-in 0.5))

(fn mousepressed [x y button] (set clicked true))

(fn mousereleased [x y button] (set init nil))

(fn update [self dt]  
  (when re-enter
    (set re-enter false)
    (enter))
  (fade.update dt))

(local keybindings (require :keybindings))
(fn keypressed [self key]
  (match key
    _ (keybindings key)))

(fn wheelmoved [self x y]
  (set scroll (math.min (- height (* 2 card-height) -50 ) (math.max 0 (+ (* -10 y) scroll)))))

{: draw : update : keypressed : mousepressed : mousereleased : wheelmoved : enter}
