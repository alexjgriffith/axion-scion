(local repl (require :lib.stdio))
(local pallet (require :pallet))
(local game-modes (require :lib.game-modes))
(local cards (require :cards))
(local {: card-draw : card-width : card-height} (require :render-card))

(local keybindings (require :keybindings))

(local lg love.graphics)

(local icons (require :icons))


;; 780 x 405

;; 

(var card-name false)
(var card-type false)
(var chance false)
(var effects false)
(var strata false)

(var timer 0)
(var period 1)
(var step 1)
(fn turn-on [dt]  
  (set timer (+ timer dt))
  (when (> timer period)
    (set timer 0)
    (match step
      1 (set card-name true)
      2 (set card-type true)
      3 (set chance true)
      4 (set effects true)
      5 (set strata true))
    (set step (+ 1 step))
    )
  (when (> step 6)
    (set card-name false)
    (set card-type false)
    (set chance false)
    (set effects false)
    (set strata false)
    (set step 1))
  )

(fn draw []
  (lg.setColor pallet.light-blue)
  ;;(love.graphics.rectangle :line 0 0 720 405)
  (love.graphics.translate 60 0)
  (local scale 1.75)
  (lg.push)
  (love.graphics.scale scale)
  (card-draw :fly-by 0 0 false 5 30)

  (local ox-c2 (- (/ (- 720 (* scale card-width)) scale) 5))
  (card-draw :conscription 0 0 false  ox-c2 30 )
  (lg.pop)
  (local {:display font} (require :fonts))
  (lg.setFont font)
  (lg.setColor pallet.light-blue)    
  (lg.setColor pallet.hot-pink)
  (lg.setColor pallet.shadow)
  ;; (lg.setColor pallet.red)
  (lg.setLineWidth 3)
  (when card-name
    (let [y 114]
      (lg.printf "Card Name" 0 30 720 :center)
      (lg.rectangle :line (+ 10 (/ (- (* scale card-width) 100 ) 2)) y 100 30 5)
      (lg.line (+ 10 90 (/ (- (* scale card-width) 80 ) 2)) (+ y 15)  260 50)
      (let [w 200]
        (lg.line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) (+ y 15)
                 460 50)
        (lg.rectangle :line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) y w 30 5))))
  
  (when card-type
      (lg.printf "Card Type" 0 110 720 :center)
    (let [y 143 h 25]
    (lg.rectangle :line (+ 10 (/ (- (* scale card-width) 100 ) 2)) y 100 h 5)
    (lg.line (+ 10 90 (/ (- (* scale card-width) 80 ) 2)) (+ y 15)  260 (+ 80 50))
    (let [w 200]
      (lg.rectangle :line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) y w h 5)
      (lg.line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) (+ y 15)
               460 (+ 80 50))
      )))
  
  (when chance
    (lg.printf "Chance" 0 190 720 :center)
    (let [y 167 h 30]
    (lg.rectangle :line (+ 10 (/ (- (* scale card-width) 100 ) 2)) y 100 h 5)
    (lg.line (+ 10 90 (/ (- (* scale card-width) 80 ) 2)) (+ y 15)
             290 (+ 160 50))
    (let [w 200]
      (lg.rectangle :line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) y w h 5)
      (lg.line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) (+ y 15)
               440 (+ 160 50))
      )))
  

  (when effects
    (lg.printf "Effects" 0 270 720 :center)
  (let [y 197 h 95]    
    (let [w 200]
      (lg.rectangle :line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) y w h 5)
      (lg.line (+ (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) (+ y 50)
               440 (+ 80 160 50)))))

  (when strata
    (lg.printf "Strata" 0 350 720 :center)
  (let [y 292 h 40]
    (let [w 50]
      (lg.line (+ 120 w 10 (/ (- (* scale card-width) 100 ) 2)) (+ y 15)
               290 (+ 160 160 50))
      (lg.rectangle :line (+ 120 10 (/ (- (* scale card-width) 100 ) 2)) y w h 5)
    
      (lg.rectangle :line (+ 100 (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) y w h 5)
            (lg.line (+ 100 (* scale ox-c2) (/ (- (* scale card-width) w ) 2)) (+ y 15)
               440 (+ 160 160 50)))))
  
  (love.graphics.setLineWidth 1)    
  )

(fn update [obj dt]
  (turn-on dt)
  )


(var background pallet.light-blue)
(fn enter [obj]
  (set background [(love.graphics.getBackgroundColor)])
  (lg.setBackgroundColor pallet.light-blue))

(fn leave [obj]
  (lg.setBackgroundColor background))


(fn keypressed [obj key]
  (local keybindings (require :keybindings))
  (keybindings key))

{: draw : update : keypressed : enter : leave}
