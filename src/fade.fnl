(local pallet (require :pallet))

(local flux (require :lib.flux))
(local fg (flux.group))

(local fade {:color (lume.clone pallet.light-blue)
             :update (fn [dt] (fg:update dt))})

(fn fade.set-color [color-st]
  (tset fade :color (lume.clone (. pallet color-st))))

(fn fade.timed-fade-in [period callback]
  (tset fade.color 4 1)
  (let [tween (fg:to fade.color period {4 0})]
    (tween:ease :quadinout)
    (when callback
      (tween:oncomplete callback))))

(fn fade.timed-fade-out [period callback]
  (tset fade.color 4 0)
  (let [tween (fg:to fade.color period {4 1})]
    (tween:ease :quadinout)
    (when callback
      (tween:oncomplete callback))))

(fn fade.draw [w h]
  (love.graphics.setColor fade.color)
  (love.graphics.rectangle :fill 0 0 w h))

fade
