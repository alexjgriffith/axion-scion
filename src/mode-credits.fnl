(local repl (require :lib.stdio))

(local pallet (require :pallet))

(local anchor (require :lib.anchor))

(local game-modes (require :lib.game-modes))

(local fade (require :fade))

(local {: title-font : text-font : card-chance-font} (require :fonts))

(var clicked false)

(local {: sounds} (require :sounds))

(var over false)

(local button (require :button))

(local credits1 "
Game Code - Alexander Griffith (GPL3+)
Music - Alexander Griffith (CCBY 4)
Font - Source Code Pro - OFL
Font - Edge Of The Galaxy - OFL
Icons - Noun Project (CCBY 4)
Sounds Effects -  NeadSimic (CCBY 3/4)
Library (Lume,Flux) - RXI (MIT/X11)
Engine (LÖVE) - LÖVE Dev Team (Zlib)
Fennel - Calvin Rose (MIT/X11)
Web Support (Love.js) Davidobot")

(local credits2 "")

(fn draw []
  (local (w h) (love.window.getMode))
  (local (mx my) (love.mouse.getPosition))
  (anchor.push-anchor 0 0 w h :center :center)
  (anchor.push-column 0 60 960 720 :center 10)    
  (anchor.push-element 0 0 960 80)  
  (love.graphics.setColor pallet.shadow)
  (love.graphics.setFont title-font)
  (love.graphics.printf "Credits" 0 0 960 :center)
  (anchor.pop)
  (anchor.push-row 0 0 960 480 :center 10)
  (anchor.push-element 0 0 480 460)  
  (love.graphics.setColor pallet.shadow)
  (love.graphics.setFont card-chance-font)
  (love.graphics.printf credits1 50 0 (- 480 100) :left)
  (anchor.pop)
  (anchor.push-element 0 0 480 460)  
  (love.graphics.setColor pallet.shadow)
  (love.graphics.setFont card-chance-font)
  (love.graphics.printf credits2 50 0 (- 480 100) :left)
  (anchor.pop)  
  (anchor.pop)
  
  (local [o who]
         (-> [false nil]
             (button "Back"
                     (fn []
                       (fade.timed-fade-out
                        0.5
                        (fn [] (game-modes.switch (require :mode-opening)))))
                     mx my clicked)))
  (when (~= o over)
    (set over o)
    (when over
      (let [opts [sounds.ding1 sounds.ding2 sounds.ding3 sounds.ding4]
            sound (. opts (math.random 1 4))]
        (sound)
        
        )))  
  (anchor.pop)
  (anchor.pop)
  (fade.draw w h)
  (set clicked false)
  )


(fn enter [self from arg]  
  (fade.timed-fade-in 0.5))

(fn mousepressed [x y button] (set clicked true))

(fn update [self dt]
  (fade.update dt))

(local keybindings (require :keybindings))
(fn keypressed [self key]
  (match key
    _ (keybindings key)))

{: draw : update : keypressed : mousepressed : enter}
