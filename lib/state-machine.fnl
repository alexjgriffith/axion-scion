;; http://gameprogrammingpatterns.com/state.html

(fn get-state-name [state-machine]
  (. state-machine.state-stack (# state-machine.state-stack)))

(fn get-state [state-machine]
  (. state-machine.states (get-state-name state-machine)))

(fn handle-input [state-machine input]
  (let [state-stack state-machine.state-stack
        states state-machine.states
        state-stack-size (# state-stack)
        current-state-name (. state-stack state-stack-size)
        current-state (. states current-state-name)
        (outcome next-state-name) (current-state:handle-input input)]
    (when outcome
      (let [next-state
          (match outcome
            :replace-state
            (let [next-state (. states next-state-name)]
              (tset state-stack state-stack-size next-state-name)
              next-state)

            :push-state
            (let [next-state (. states next-state-name)]
              (tset state-stack (+ 1 state-stack-size) next-state-name)
              next-state)

            :pop-state
            (let [last-state-name (. state-stack (- state-stack-size 1))
                  last-state (. states last-state-name)]
              (tset state-stack state-stack-size nil)
              (tset state-stack (- state-stack-size 1) last-state-name)
              last-state)

            _ {})]
      (when current-state.exit (current-state:exit))
      (when next-state.enter (next-state:enter current-state))
    ;;(values current-state (get-state-name state-machine))
    (values outcome next-state-name current-state (get-state-name state-machine))))
    ))

(fn add-state [state-machine name obj]
  (tset state-machine.states name obj))

(fn remove-state [state-machine name]
  (table.remove state-machine.states name))

(fn update [state-machine dt ...]
  (let [current-state (get-state state-machine)]
    (current-state:update dt ...)))

(local state-machine {: handle-input : update  : add-state : remove-state
                      : get-state-name})

(local state-machine-mt {:__index state-machine})

(fn init [current-state ...]
  (let [sm (setmetatable {:states [] :state-stack []} state-machine-mt)]
    (sm:add-state current-state.name current-state)
    (each [_ state (ipairs [...])]
      (sm:add-state state.name state))
    (table.insert sm.state-stack current-state.name)
    sm))

{: init}
